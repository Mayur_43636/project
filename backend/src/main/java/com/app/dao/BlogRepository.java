package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.PathVariable;

import com.app.pojos.Blog;
import com.app.pojos.Comment;

public interface BlogRepository extends JpaRepository<Blog,Integer>{
	@Query("select b from Blog b where b.status='VERIFYING'")
	List<Blog> getSubmittedBlogs();
	
	@Query("select b from Blog b where b.status='APPROVED'")
	List<Blog> getApprovedBlogs();
	
	@Query("select b from Blog b where b.category=:category and b.status='APPROVED'")
	List<Blog> getBlogsByCategory(String category);
	
	@Query("select b from Blog b left outer join fetch b.users where b.users.id=:id")
	List<Blog> getPublishedBlogByUser(int id);
}
