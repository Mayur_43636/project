package com.app.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.dto.LoginDTO;
import com.app.dto.ResponseDTO;
import com.app.dto.UserDTO;
import com.app.pojos.Blog;
import com.app.pojos.Role;
import com.app.pojos.Status;
import com.app.pojos.User;
import com.app.service.IUserService;

import lombok.experimental.Helper;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {
	
	@Value("${file.upload.location}")
	private String location;
	//dependency
	@Autowired
	private IUserService userService;
	
	@PostMapping("/signup")
	private ResponseDTO<?> addUserDetails(@RequestBody UserDTO userDTO){
		System.out.println("in add user details ");
		return new ResponseDTO<>(HttpStatus.OK,"User details added",userService.addUserDetails(userDTO));
	}
	
	@PostMapping("/signin")
	private ResponseDTO<?> getUserDetails(@RequestBody LoginDTO loginDTO){
		System.out.println("in get user details "+loginDTO.getEmail() +" "+loginDTO.getPassword());
		User u = userService.getUserDetails(loginDTO.getEmail(), loginDTO.getPassword());
		System.out.println("user "+ u);
		return new ResponseDTO<>(HttpStatus.OK, "User found", u);
	}
	
	
	//REST request handling method for 
	@GetMapping("/profile")
	public ResponseDTO<?>getUserDetails(@RequestHeader int token)
	{
		System.out.println("in get user details "+token);
		return new ResponseDTO<>(HttpStatus.OK,"sending user details",userService.getUserDetails(token));
	}
	
	@PostMapping("/profile")
	public ResponseDTO<?> updateUserDetails(@RequestParam MultipartFile imageFile, @RequestParam String name,
			@RequestParam String email, @RequestParam String password, @RequestParam String about,
			@RequestParam String gender, @RequestHeader int token) {
		User user = new User();
		try {
			String newFilename = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg";
			imageFile.transferTo(new File(location, newFilename));
			System.out.println("new file name : " + newFilename);
			user.setId(token);
			user.setName(name);
			user.setEmail(email);
			user.setPassword(password);
			user.setAbout(about);
			user.setGender(gender);
			user.setRole(Role.BLOGGER);
			user.setPhoto(newFilename);
			System.out.println("user : " + user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseDTO<>(HttpStatus.OK, "success", userService.updateUserDetails(user));
	}
	
	@GetMapping("/image/{imgName}")
	// Sample URL : http://localhost:8080/image/imageName.extension
	public ResponseEntity<InputStreamResource> getImage(@PathVariable String imgName) throws IOException {

		System.out.println("in img download 2 " + (location + imgName));
		Path path = Paths.get(location, imgName);
		InputStreamResource inputStreamResource = new InputStreamResource(new FileInputStream(path.toFile()));

		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type", Files.probeContentType(path));
		return ResponseEntity.ok().headers(headers).body(inputStreamResource);
	}
}

