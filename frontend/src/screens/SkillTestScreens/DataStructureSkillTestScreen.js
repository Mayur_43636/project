import React, { useState } from 'react';

const DataStructureSkillTest = () => {
    const questions = [
        {
            questionText: 'How can we describe an array in the best possible way?',
            answerOptions: [
                { answerText: 'The Array shows a hierarchical structure.', isCorrect: false },
                { answerText: 'Arrays are immutable.', isCorrect: false },
                { answerText: 'Container that stores the elements of similar types', isCorrect: true },
                { answerText: 'The Array is not a data structure', isCorrect: false },
            ],
        },
        {
            questionText: 'How can we initialize an array in C language?',
            answerOptions: [
                { answerText: 'int arr[2]=(10, 20)', isCorrect: false },
                { answerText: 'int arr(2)={10, 20}', isCorrect: false },
                { answerText: 'int arr[2] = {10, 20}', isCorrect: true },
                { answerText: 'int arr(2) = (10, 20)', isCorrect: false },
            ],
        },
        {
            questionText: 'Which of the following is the advantage of the array data structure?',
            answerOptions: [
                { answerText: 'Elements of mixed data types can be stored.', isCorrect: false },
                { answerText: 'Easier to access the elements in an array', isCorrect: true },
                { answerText: 'Index of the first element starts from 1.', isCorrect: false },
                { answerText: 'Elements of an array cannot be sorted', isCorrect: false },
            ],
        },
        {
            questionText: 'Which of the following highly uses the concept of an array?',
            answerOptions: [
                { answerText: 'Binary Search tree', isCorrect: false },
                { answerText: 'Caching', isCorrect: false },
                { answerText: 'Spatial locality', isCorrect: true },
                { answerText: 'Scheduling of Processes', isCorrect: false },
            ],
        },
        {
            questionText: 'Which of the following is the disadvantage of the array?',
            answerOptions: [
                { answerText: 'Stack and Queue data structures can be implemented through an array.', isCorrect: false },
                { answerText: 'Index of the first element in an array can be negative', isCorrect: false },
                { answerText: 'Wastage of memory if the elements inserted in an array are lesser than the allocated size', isCorrect: true },
                { answerText: 'Elements can be accessed sequentially.', isCorrect: false },
            ],
        },
    ];

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [showScore, setShowScore] = useState(false);
    const [score, setScore] = useState(0);

    const handleAnswerOptionClick = (isCorrect) => {
        if (isCorrect) {
            setScore(score + 1);
        }

        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < questions.length) {
            setCurrentQuestion(nextQuestion);
        } else {
            setShowScore(true);
        }
    };
    return (
        <>
            <div className="container" style={{ 'margin-top': '40px' }}>
                <div className='skill'>
                    {showScore ? (
                        <div className='score-section'>
                            You scored {score} out of {questions.length}
                        </div>
                    ) : (
                        <>
                            <div className='question-section'>
                                <div className='question-count'>
                                    <span>Question {currentQuestion + 1}</span>/{questions.length}
                                </div>
                                <div className='question-text'>{questions[currentQuestion].questionText}</div>
                                <div className='answer-section'>
                                    {questions[currentQuestion].answerOptions.map((answerOption) => (
                                        <button onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
                                    ))}
                                </div>
                            </div>

                        </>
                    )}
                </div>
            </div>
        </>
    );
}

export default DataStructureSkillTest