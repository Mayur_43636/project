package com.app.pojos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "blogs")
public class Blog extends BaseEntity {
	
	@Column(length = 100)
	private String title;
	@Column(length = 20)
	private String category;
	@Column(length = 255)
	private String description;
	@Lob
	@Column(name = "content", length = 1000)
	private String content;
	@Column(length = 255)
	private String photo;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate date;
	@Enumerated(EnumType.STRING)
	private Status status;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false)
    private User users;

	@JsonIgnore
	@OneToMany(mappedBy="blogs",cascade=CascadeType.ALL,orphanRemoval = true) 
	private List<Comment> comments= new ArrayList<Comment>();
	 

	public Blog() {
		System.out.println("in ctor of : " + getClass().getName());
	}

	public Blog(String title, String category, String description, String content, String photo, LocalDate date,
			Status status) {
		super();
		this.title = title;
		this.category = category;
		this.description = description;
		this.content = content;
		this.photo = photo;
		this.date = date;
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	

	public User getUser() {
		return users;
	}

	public void setUser(User users) {
		this.users = users;
	}

	
	public User getUsers() {
		return users;
	}

	public void setUsers(User users) {
		this.users = users;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public void addComment(Comment comment) {
		comments.add(comment);
		comment.setBlogs(this);
	}
	public void removeComment(Comment comment) {
		comments.remove(comment);
		comment.setBlogs(null);
	}
	
	@Override
	public String toString() {
		return "Blog [id=" + getId() + ", title=" + title + ", category=" + category + ", description=" + description
				+ ", content=" + content + ", photo=" + photo + ", date=" + date + ", status=" + status + "]";
	}
}
