import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"

import { deleteBlog,getPublishedBlogByUser}from '../actions/blogActions'
const PublishedBlogScreen = () => {

    const dispatch = useDispatch()
    const blogs = useSelector((store) => store.getblog)
    const { loading, response, error } = blogs
    const deleteblog= useSelector((store) => store.deleteBlog)

    //call once when page is loaded
    useEffect(() => {
        dispatch(getPublishedBlogByUser(sessionStorage.getItem('token')))
    }, [])

    useEffect(() => {

    }, [loading, response, error])

    useEffect(() => {
        console.log('')
        console.log(deleteblog.response)
        if(deleteblog.response && (deleteblog.response.status == 'OK')){
        console.log(deleteblog.response.result)
        dispatch(getPublishedBlogByUser())
    }
}, [deleteblog.loading,deleteblog.response,])

const onDelete = (id) => {
    dispatch(deleteBlog(id))
    console.log('ondelete')
}

    if (response)
        console.log('response', response.result)

       

    const imageUrl = "http://localhost:8080/blog/image/"
    return (
        <div>
            <div className="main">
                <h1>Published Blogs</h1>
                <ul className="cards">
                    {response && response.result && response.result.length > 0 &&
                        response.result.map((blog) => {
                            return (
                                <li className="cards_item">
                                    <div className="card">
                                        <div className="card_image"><img src={imageUrl + blog.photo} /></div>
                                        <div className="card_content">
                                            <h2 className="card_title">{blog.title}</h2>
                                            <p className="card_text">{blog.description}</p>
                                            <Link to="/blog_display">
                                                <button onClick={() => sessionStorage.setItem('blog_id', blog.id)} className="neha card_btn">Read More</button>
                                            </Link>
                                            <button onClick={() => {
                                                onDelete(blog.id)
                                            }}
                                                className="neha card_btn" >
                                                Delete
                                        </button>
                                        </div>
                                    </div>
                                </li>
                            )
                        })}
                </ul>
            </div>
        </div>

    )

}
export default PublishedBlogScreen