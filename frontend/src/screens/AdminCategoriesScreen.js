import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { getCategory, deleteCategory } from '../actions/categoryActions'

const AdminCategoriesScreen = (props) => {

    const dispatch = useDispatch()
    const category = useSelector((store) => store.getCategory)
    const { loading, response, error } = category

    const deletecategory = useSelector((store) => store.deleteCategory)

    //call once when page loaded successfully
    useEffect(() => {
        dispatch(getCategory())
    }, [])

    useEffect(() => { }, [loading, response, error])

    useEffect(() => {
        console.log('use effect called for delete category')
        console.log(deletecategory.response)
         //if delete category is success rerender category list
        if(deletecategory.response && (deletecategory.response.status == 'OK')){
         console.log(deletecategory.response.result)
         dispatch(getCategory())
     }
     }, [deletecategory.loading,deletecategory.response,])
 
     const onDelete = (id) => {
         dispatch(deleteCategory(id))
         console.log('ondelete')
     } 

    return (
        <div>
            <div className="container text-center" style={{ 'margin-top': '30px' }}>
                <Link to="/add_category">
                    <button className="btn btn-primary">Add Category</button>
                </Link>
            </div>
            <div className="container">
                <div className="row">
                    {response && response.result && response.result.length > 0 &&
                        response.result.map((category) => {
                            return (
                                <div className="category col-md-3">
                                    <div className="title">{category.title}</div>
                                    <div className="description">{category.description}</div>
                                    <Link to={{
                                        pathname: "/update_category",
                                        state: category
                                    }}>
                                    <button
                                        className="btn btn-sm btn-success btn-add-to-cart" style={buttonStyle}>
                                        Update
                                    </button>
                                    </Link>
                                    <button onClick={() => {
                                        onDelete(category.id)
                                    }}
                                        className="btn btn-sm btn-success btn-add-to-cart" style={buttonStyle}>
                                        Delete
                                </button>
                                </div>
                            )
                        })}
                </div>
            </div>
        </div>
    )
}

const buttonStyle = {
    margin: '10px'
}
export default AdminCategoriesScreen
