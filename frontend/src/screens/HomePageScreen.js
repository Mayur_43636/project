import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { getPublishedBlog } from "../actions/blogActions"


const HomePageScreen = () => {
  
  const dispatch = useDispatch()
  const blogs = useSelector((store) => store.getblog)
  const { loading, response, error } = blogs

  //call once when page is loaded
  useEffect(() => {
      dispatch(getPublishedBlog())
  }, [])

  useEffect(() => {

  }, [loading, response, error])

  if(response)
      console.log('response', response.result)

  const imageUrl = "http://localhost:8080/blog/image/"

  return (
    <div>
      <div className="container">
        <div className="gallery-title centered">
          <div className="section-title">
            <h2>Welcome to Blog Development</h2>
          </div>
          <div className="title-text">
            <p>Content builds relationships. Relationships are built on trust. Trust drives revenue.</p>
          </div>
        </div>
      </div>
    
      {response && response.result && response.result.length > 0 &&
                        response.result.map((blog) => { 
                            return (
      <div class="card_n">
        <div class="image_n">
          <img src={imageUrl + blog.photo} />
        </div>
        <div class="title_n">
          <h1 class="h1_n">
          {blog.title}</h1>
        </div>
        <div class="des_n">
          <p>{blog.description}</p>
          <Link to="/blog_display">
          <button onClick={() => sessionStorage.setItem('blog_id',blog.id)} class="button_n">Read More...</button>
          </Link>
        </div>
      </div>
       )
      })}

      
      </div>
  )


}
export default HomePageScreen


