package com.app.service;

import java.util.List;

import com.app.dto.NewsDTO;
import com.app.pojos.News;

public interface INewsService {
	News addNewsDetails(NewsDTO newsDTO);
	List<News> getNewsdetails();
	void deleteNews(int id);
}
