import { WEBINAR_ADD_FAIL, 
    WEBINAR_ADD_REQUEST, 
    WEBINAR_ADD_RESET, 
    WEBINAR_ADD_SUCCESS, 
    WEBINAR_DELETE_FAIL, 
    WEBINAR_DELETE_REQUEST, 
    WEBINAR_DELETE_RESET, 
    WEBINAR_DELETE_SUCCESS, 
    WEBINAR_FETCH_FAIL, 
    WEBINAR_FETCH_REQUEST, 
    WEBINAR_FETCH_RESET, 
    WEBINAR_FETCH_SUCCESS } from "../constants/webinarConstants"

export const fetchWebinarReducer = (state = {}, action) => {
    switch (action.type) {
      case WEBINAR_FETCH_REQUEST:
        return { loading: true }
      case WEBINAR_FETCH_SUCCESS:
        return { loading: false, response: action.payload }
      case WEBINAR_FETCH_FAIL:
        return { loading: false, error: action.payload }
      case WEBINAR_FETCH_RESET:
        return {}
      default:
        return state
    }
  }

  export const addWebinarReducer = (state = {}, action) => {
    switch (action.type) {
      case WEBINAR_ADD_REQUEST:
        return { loading: true }
      case WEBINAR_ADD_SUCCESS:
        return { loading: false, response: action.payload }
      case WEBINAR_ADD_FAIL:
        return { loading: false, error: action.payload }
      case WEBINAR_ADD_RESET:
        return {}
      default:
        return state
    }
  }

  export const deleteWebinarReducer = (state = {}, action) => {
    switch (action.type) {
      case WEBINAR_DELETE_REQUEST:
        return { loading: true }
      case WEBINAR_DELETE_SUCCESS:
        return { loading: false, response: action.payload }
      case WEBINAR_DELETE_FAIL:
        return { loading: false, error: action.payload }
      case WEBINAR_DELETE_RESET:
        return {}
      default:
        return state
    }
  }