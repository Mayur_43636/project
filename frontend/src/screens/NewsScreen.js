import { Link } from "react-router-dom"
import Header from "../components/Header"
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from "react"
import { getNews } from "../actions/newsActions"


const NewsScreen = () => {
    
    const dispatch = useDispatch()
    const news = useSelector((store) => store.getNews)
    const { loading, response, error } = news

     //call once when page loaded successfully
     useEffect(() => {
        dispatch(getNews())
    }, [])

    useEffect(() => { }, [loading, response, error])
    //console.log(window.location.pathname)


    return (
        <div className="container">
            <div className="row">
                <Header title="News" />
                {response && response.result && response.result.length > 0 &&
                    response.result.map((news) => {
                        return (
                        <div className="news col-md-3">
                            <div className="title">{news.title}</div>
                            <div className="description">{news.description}</div>
                            <div className="date">date: {news.date}</div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

const buttonStyle = {
    margin: '10px'
}

export default NewsScreen
