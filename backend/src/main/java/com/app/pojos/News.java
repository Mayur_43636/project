package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "news")
public class News extends BaseEntity {
	@Column(length = 50)
	private String title;
	@Lob
	@Column(name = "description", length = 1000)
	private String description;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate date;
	
	public News() {
		System.out.println("in ctor of : "+getClass().getName());
	}

	public News(String title, String description, LocalDate date) {
		super();
		this.title = title;
		this.description = description;
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "News [id=" + getId() + ", title=" + title + ", description=" + description + ", date=" + date + "]";
	}
}
