import {
    NEWS_ADD_REQUEST,
    NEWS_ADD_SUCCESS,
    NEWS_ADD_FAIL,
    NEWS_ADD_RESET,
    NEWS_FETCH_REQUEST,
    NEWS_FETCH_SUCCESS,
    NEWS_FETCH_FAIL,
    NEWS_FETCH_RESET,
    NEWS_DELETE_FAIL, 
    NEWS_DELETE_REQUEST, 
    NEWS_DELETE_RESET, 
    NEWS_DELETE_SUCCESS,
  } from '../constants/newsConstants'
  
  export const addNewsReducer = (state = {}, action) => {
    switch (action.type) {
      case NEWS_ADD_REQUEST:
        return { loading: true }
      case NEWS_ADD_SUCCESS:
        return { loading: false, response: action.payload }
      case NEWS_ADD_FAIL:
        return { loading: false, error: action.payload }
      case NEWS_ADD_RESET:
        return {}
      default:
        return state
    }
  }
  
  export const fetchNewsReducer = (state = {}, action) => {
    switch (action.type) {
      case NEWS_FETCH_REQUEST:
        return { loading: true }
      case NEWS_FETCH_SUCCESS:
        return { loading: false, response: action.payload }
      case NEWS_FETCH_FAIL:
        return { loading: false, error: action.payload }
      case NEWS_FETCH_RESET:
        return {}
      default:
        return state
    }
  }
  
  export const deleteNewsReducer = (state = {}, action) => {
    switch (action.type) {
      case NEWS_DELETE_REQUEST:
        return { loading: true }
      case NEWS_DELETE_SUCCESS:
        return { loading: false, response: action.payload }
      case NEWS_DELETE_FAIL:
        return { loading: false, error: action.payload }
      case NEWS_DELETE_RESET:
        return {}
      default:
        return state
    }
  }