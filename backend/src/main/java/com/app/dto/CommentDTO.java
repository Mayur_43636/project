package com.app.dto;

public class CommentDTO {
	private int id;
	private String name;
	private String description;
	
	public CommentDTO() {
		System.out.println("in ctor of : "+getClass().getName());
	}

	public CommentDTO(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "CommentDTO [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
}
