package com.app.controller;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.CommentDTO;
import com.app.dto.ResponseDTO;
import com.app.pojos.Comment;
import com.app.service.IBlogService;
import com.app.service.ICommentService;
import com.app.service.IUserService;

@RestController
@RequestMapping("/comment")
@CrossOrigin
public class CommentController {
	
	@Autowired
	private IBlogService blogService;
	
	@Autowired
	private ICommentService commentService;

	@PostMapping("/add/{id}")
	public ResponseDTO<?> addComment(@PathVariable int id,@RequestBody Comment comment){
		System.out.println("in comment controller ");
		return new ResponseDTO<>(HttpStatus.OK, "comment added",commentService.addComment(id, comment));
	}
	
	@GetMapping("/list/{id}")
	public ResponseDTO<?> addComment(@PathVariable int id){
		System.out.println("in comment controller ");
		return new ResponseDTO<>(HttpStatus.OK, "comment added",commentService.getCommentList(id));
	}
	
	
	
}
