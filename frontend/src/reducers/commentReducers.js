import { COMMENT_ADD_FAIL, 
    COMMENT_ADD_REQUEST, 
    COMMENT_ADD_RESET, 
    COMMENT_ADD_SUCCESS, 
    COMMENT_FETCH_FAIL, 
    COMMENT_FETCH_REQUEST, 
    COMMENT_FETCH_RESET, 
    COMMENT_FETCH_SUCCESS } from "../constants/commentConstants"

export const fetchCommentReducer = (state = {}, action) => {
    switch (action.type) {
      case COMMENT_FETCH_REQUEST:
        return { loading: true }
      case COMMENT_FETCH_SUCCESS:
        return { loading: false, response: action.payload }
      case COMMENT_FETCH_FAIL:
        return { loading: false, error: action.payload }
      case COMMENT_FETCH_RESET:
        return {}
      default:
        return state
    }
  }

  export const addCommentReducer = (state = {}, action) => {
    switch (action.type) {
      case COMMENT_ADD_REQUEST:
        return { loading: true }
      case COMMENT_ADD_SUCCESS:
        return { loading: false, response: action.payload }
      case COMMENT_ADD_FAIL:
        return { loading: false, error: action.payload }
      case COMMENT_ADD_RESET:
        return {}
      default:
        return state
    }
  }