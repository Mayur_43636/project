import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { deleteWebinar, getWebinars } from '../actions/webinarActions'


const AdminWebinarScreen = (props) => {

    const dispatch = useDispatch()
    const webinars = useSelector((store) => store.getWebinars)
    const { loading, response, error } = webinars

    const deletewebinar = useSelector((store) => store.deleteWebinar)
   
    //call once when page loaded successfully
    useEffect(() => {
        dispatch(getWebinars())
    }, [])

    useEffect(() => { }, [loading, response, error])

    
    useEffect(() => {
       console.log('use effect called for delete webinar')
       console.log(deletewebinar.response)
        //if delete webinar is success rerender webinar list
       if(deletewebinar.response && (deletewebinar.response.status == 'OK')){
        console.log(deletewebinar.response.result)
        dispatch(getWebinars())
    }
    }, [deletewebinar.loading,deletewebinar.response,])

    const onDelete = (id) => {
        dispatch(deleteWebinar(id))
        console.log('ondelete')
    } 
    return (
        <div>
            <div className="container text-center" style={{ 'marginTop': '30px' }}>
                <Link to="/add_webinar">
                    <button className="btn btn-primary">Add Webinar</button>
                </Link>
            </div>
            <div className="container">
                <div className="row">
                    {response && response.result && response.result.length > 0 &&
                        response.result.map((webinar) => {
                            return (
                                <div className="webinar col-md-3">
                                    <div className="title">{webinar.title}</div>
                                    <div className="description">{webinar.description}</div>
                                    <div className="date">date: {webinar.date}</div>
                                    <Link to={{
                                        pathname: "/update_webinar",
                                        state: webinar
                                    }}>
                                    <button
                                        className="btn btn-sm btn-success btn-add-to-cart" style={buttonStyle}>
                                        Update
                                    </button>
                                    </Link>
                                    <button onClick={() => {
                                        onDelete(webinar.id)
                                    }}
                                        className="btn btn-sm btn-success btn-add-to-cart" style={buttonStyle}>
                                        Delete
                                </button>
                                </div>
                            )
                        })}
                </div>
            </div>
        </div>
    )
}

const buttonStyle = {
    margin: '10px'
}
export default AdminWebinarScreen