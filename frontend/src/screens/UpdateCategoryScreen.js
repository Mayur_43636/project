import { useEffect, useLayoutEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { addCategory } from "../actions/categoryActions"

const UpdateCategoryScreen = (props) => {
    const category = props.location.state
    const [id, setId] = useState(category.id)
    const [title, setTitle] = useState(category.title)
    const [description, setDescription] = useState(category.description)

    const addcategory = useSelector((store) => store.addCategory)
    const { loading, response, error } = addcategory

    const dispatch = useDispatch() 

    const onUpdate = () => {
        console.log('id', id)
        console.log('title', title)
        console.log('description', description)
        dispatch(addCategory(id,title,description))
    }

    //console.log(props.location.state)

    useEffect(() => {
        //successfull updation of category
        if(response && (response.status == 'OK')){ 
            console.log(response.result)
            props.history.push('/admin_category')
            window.location.reload(false)
        }
         else if(error){
            //there is an error while making the api call
            console.log(error)
            alert('error while making API call')
        } 
    },[loading, error, response])

    return (
        <div>
            <div className="container">
                <div className="col-md-6 offset-md-3">
                    <div className="card">
                        <div className="card-header text-center primary-background text-white">
                            <br />
                                   Update a Category Here
                                </div>
                        <div className="card-body">
                                <div className="form-group">
                                    <label for="category_title">Title</label>
                                    <input onChange={(e) => {
                                    setTitle(e.target.value)
                                }} type="text" className="form-control" value={title} />
                                </div>
                                <div className="form-group">
                                    <label for="category_description">Description</label>
                                    <textarea onChange={(e) => {
                                    setDescription(e.target.value)
                                }} className="form-control" rows="5" value={description} ></textarea>
                                </div>
                                <br />
                                <button onClick={onUpdate} type="submit" className="btn btn-primary" style={{ 'margin-top': '20px' }}>Update Category</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const style = {
    margin: '8px'
}

export default UpdateCategoryScreen



