package com.app.service;

import java.util.List;

import com.app.dto.CommentDTO;
import com.app.pojos.Comment;

public interface ICommentService {
	
	String addComment(int id, Comment comment);
	List<CommentDTO> getCommentList(int id);
}
