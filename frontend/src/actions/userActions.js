import { USER_SIGNIN_FAIL, 
    USER_SIGNIN_REQUEST, 
    USER_SIGNIN_SUCCESS, 
    USER_SIGNUP_FAIL, 
    USER_SIGNUP_REQUEST, 
    USER_SIGNUP_SUCCESS ,
    USER_SIGNOUT,
    USER_UPDATE_PROFILE_REQUEST,
    USER_UPDATE_PROFILE_SUCCESS,
    USER_UPDATE_PROFILE_FAIL,
    USER_DETAILS_REQUEST,
    USER_DETAILS_SUCCESS,
    USER_DETAILS_FAIL,
  
    
} from '../constants/userConstants'

import axios from 'axios'

export const signup = (name, email, password, gender, about) => {
    return (dispatch) => {
        dispatch({
            type: USER_SIGNUP_REQUEST,
        })

        const header = {
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const body = {
            name, email, password, gender, about
        }

        const url = "http://localhost:8080/user/signup"
        axios
            .post(url, body, header)
            .then((response) => {
                dispatch({
                    type: USER_SIGNUP_SUCCESS,
                    payload: response.data,
                })
            })
            .catch((error) => {
                dispatch({
                    type: USER_SIGNUP_FAIL,
                    payload: error,
                })
            })
    }
}

export const signin = (email, password) => {
    return (dispatch) => {
        dispatch({
            type: USER_SIGNIN_REQUEST,
        })

        const header = {
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const body = {
            email, password, 
        }

        const url = "http://localhost:8080/user/signin"
        axios
            .post(url, body, header)
            .then((response) => {
                dispatch({
                    type: USER_SIGNIN_SUCCESS,
                    payload: response.data,
                })
            })
            .catch((error) => {
                dispatch({
                    type: USER_SIGNIN_FAIL,
                    payload: error,
                })
            })
    }
}

export const logout = () => {
    return (dispatch) => {
      sessionStorage.clear()
      dispatch({ type: USER_SIGNOUT })
      document.location.href = '/'
    }
  }
 
  
//view profileeeeeee

export const getUserDetails = () => {
  return (dispatch) => {
    dispatch({
      type: USER_DETAILS_REQUEST,
    })

    const header = {
      headers: {
        'Content-Type': 'application/json',
        token: sessionStorage['token'],
      },
    }

    const url = 'http://localhost:8080/user/profile/'
    axios
      .get(url, header)
      .then((response) => {
        dispatch({
          type: USER_DETAILS_SUCCESS,
          payload: response.data,
        })
      })
      .catch((error) => {
        dispatch({
          type: USER_DETAILS_FAIL,
          payload: error,
        })
      })
  }
}

export const updateProfile = (formdata) => {
    return (dispatch) => {
      dispatch({
        type: USER_UPDATE_PROFILE_REQUEST,
      })
  
      const header = {
        headers: {
          token: sessionStorage['token'],
        },
      }
      
      const url = 'http://localhost:8080/user/profile'
      axios
        .post(url, formdata, header)
        .then((response) => {
          dispatch({
            type: USER_UPDATE_PROFILE_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: USER_UPDATE_PROFILE_FAIL,
            payload: error,
          })
        })
    }
  }
  
