import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { signup } from '../actions/userActions'

const RegisterScreen = (props) => {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [gender, setGender] = useState('')
    const [about, setAbout] = useState('')
    const dispatch = useDispatch()

    const userSignup = useSelector((store) => store.userSignup)
    const { loading, response, error } = userSignup

    // listen on change of the [loading, response, error] values
    useEffect(() => {
        console.log('use effect called ')
        console.log('loading ',loading)
        console.log('response ',response)
        console.log('error ',error)

        if(response && (response.status == 'OK')){
            //user got registered
            console.log(response.result)
            props.history.push('/login')
        } else if(error) {
            //there is an error while making the api call
            console.log(error)
            alert('error while making API call')
        }
    }, [loading, response, error])

    const onRegister = () => {
        /* console.log('name', name)
        console.log('email', email)
        console.log('password', password)
        console.log('gender', gender)
        console.log('about', about) */
        dispatch(signup(name,email,password,gender,about))
    }

    return (
        <div>
            <div className="container" style={{ 'margin-top': '40px' }}>
                <div className="col-md-6 offset-md-3">
                    <div className="card">
                        <div className="card-header text-center primary-background text-white">
                            <span className="fa fa-3x fa-user-circle"></span>
                            <br />
                                    Register here
                        </div>
                        <div className="card-body">
                            <div className="form-group">
                                <label for="user_name" style={style}>User Name</label>
                                <input onChange={(e) => {
                                    setName(e.target.value)
                                }} className="form-control" placeholder="JohnDoe" style={style} />
                            </div>
                            <div className="form-group">
                                <label for="exampleInputEmail1" style={style}>Email address</label>
                                <input onChange={(e) => {
                                    setEmail(e.target.value)
                                }} type="email" className="form-control" placeholder="test@gmail.com" style={style} />
                                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div className="form-group">
                                <label for="exampleInputPassword1" style={style}>Password</label>
                                <input onChange={(e) => {
                                    setPassword(e.target.value)
                                }} type="password" className="form-control" placeholder="******" style={style} />
                            </div>
                            <div className="form-group">
                                <label for="gender" style={style}>Select Gender</label>
                                <br />
                                <input onChange={(e) => {
                                    setGender(e.target.value)
                                }} type="radio" id="gender" name="gender" value="Male" />Male
                                <input onChange={(e) => {
                                    setGender(e.target.value)
                                }} type="radio" id="gender" name="gender" value="Female" />Female
                                </div>
                            <div className="form-group">
                                <textarea onChange={(e) => {
                                    setAbout(e.target.value)
                                }} className="form-control" rows="5" placeholder="Enter something about yourself" style={style}></textarea>
                            </div>
                            <br />
                            <button onClick={onRegister} className="btn btn-primary">Register</button>
                        </div>
                    </div>
                </div>
            </div>
            { userSignup.loading &&  <div>waiting for result</div>}
        </div>
    )
}

const style = {
    margin: '8px'
}

export default RegisterScreen