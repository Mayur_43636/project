package com.app.service;

import com.app.dto.UserDTO;
import com.app.pojos.User;

public interface IUserService {
	User addUserDetails(UserDTO user);

	User getUserDetails(String email, String password);

	User getUserDetails(int token);

	String updateUserDetails(User user);
}
