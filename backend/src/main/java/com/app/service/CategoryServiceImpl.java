package com.app.service;


import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.CategoryRepository;
import com.app.dto.CategoryDTO;
import com.app.pojos.Category;


@Service
@Transactional
public class CategoryServiceImpl implements ICategoryService {
	
	//dependency
		@Autowired
		private CategoryRepository categoryRepo;
		
	@Override
	public Category addCategoryDetails(CategoryDTO categoryDTO) {
		Category category = new Category();
		BeanUtils.copyProperties(categoryDTO, category);
		System.out.println("add category src "+categoryDTO);
		return categoryRepo.save(category);
	}

	@Override
	public List<Category> getCategorydetails() {
		System.out.println("get category details ");
		return categoryRepo.findAll();
	}

	@Override
	public void deleteCategory(int id) {
		categoryRepo.deleteById(id);
	}

}
