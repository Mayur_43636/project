import React, { useState } from 'react';

const JavaSkillTest = () => {
    const questions = [
        {
            questionText: 'Which of the following is not a Java features?',
            answerOptions: [
                { answerText: 'Dynamic', isCorrect: false },
                { answerText: 'Architecture Neutral', isCorrect: false },
                { answerText: 'Use of pointers', isCorrect: true },
                { answerText: 'Object-oriented', isCorrect: false },
            ],
        },
        {
            questionText: '_____ is used to find and fix bugs in the Java programs.',
            answerOptions: [
                { answerText: 'JVM', isCorrect: false },
                { answerText: 'JRE', isCorrect: false },
                { answerText: 'JDK', isCorrect: false },
                { answerText: 'JDB', isCorrect: true },
            ],
        },
        {
            questionText: ' What is the return type of the hashCode() method in the Object class?',
            answerOptions: [
                { answerText: 'Object', isCorrect: false },
                { answerText: 'int', isCorrect: true },
                { answerText: 'long', isCorrect: false },
                { answerText: 'void', isCorrect: false },
            ],
        },
        {
            questionText: 'Which of the following for loop declaration is not valid?',
            answerOptions: [
                { answerText: 'for ( int i = 99; i >= 0; i / 9 )', isCorrect: true },
                { answerText: 'for ( int i = 7; i <= 77; i += 7 )', isCorrect: false },
                { answerText: 'for ( int i = 20; i >= 2; - -i )', isCorrect: false },
                { answerText: 'for ( int i = 2; i <= 20; i = 2* i )', isCorrect: false },
            ],
        },
        {
            questionText: 'Which method of the Class.class is used to determine the name of a class represented by the class object as a String?',
            answerOptions: [
                { answerText: 'getClass()', isCorrect: true },
                { answerText: 'intern()', isCorrect: false },
                { answerText: 'getName()', isCorrect: false },
                { answerText: 'toString()', isCorrect: false },
            ],
        },
    ];

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [showScore, setShowScore] = useState(false);
    const [score, setScore] = useState(0);

    const handleAnswerOptionClick = (isCorrect) => {
        if (isCorrect) {
            setScore(score + 1);
        }

        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < questions.length) {
            setCurrentQuestion(nextQuestion);
        } else {
            setShowScore(true);
        }
    };
    return (
        <>
            <div className="container" style={{ 'margin-top': '40px' }}>
                <div className='skill'>
                    {showScore ? (
                        <div className='score-section'>
                            You scored {score} out of {questions.length}
                        </div>
                    ) : (
                        <>
                            <div className='question-section'>
                                <div className='question-count'>
                                    <span>Question {currentQuestion + 1}</span>/{questions.length}
                                </div>
                                <div className='question-text'>{questions[currentQuestion].questionText}</div>
                                <div className='answer-section'>
                                    {questions[currentQuestion].answerOptions.map((answerOption) => (
                                        <button onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
                                    ))}
                                </div>
                            </div>

                        </>
                    )}
                </div>
            </div>
        </>
    );
}

export default JavaSkillTest