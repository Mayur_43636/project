import { useEffect, useState, } from "react"
import { Link } from "react-router-dom"
import { useDispatch, useSelector } from 'react-redux'
import { signin } from "../actions/userActions"

const LoginScreen = (props) => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const userSignin = useSelector((store) => store.userSignin)
    const { loading, response, error } = userSignin

    const dispatch = useDispatch()

    const onLogin = () => {
        /*   console.log('email', email)
          console.log('password', password) */
        dispatch(signin(email, password))
    }

    useEffect(() => {
        //successfull login
        if (response && (response.status == 'OK')) {
            sessionStorage.setItem("user_role", response.result.role)
            sessionStorage.setItem("token", response.result.id)
            sessionStorage.setItem("user_name",response.result.name)
            console.log(sessionStorage.getItem("user_role"))
            console.log(response.result)
            //props.history.push('/')
            document.location.href = '/'
        }
        else if (response && response.status != 'OK') {
            alert(response.error)
        } else if (error) {
            alert(error)
        }
    }, [loading, error, response])

    return (

        <div className="container" style={{ 'margin-top': '40px' }}>
            <div className="row">
                <div className="col-md-4 offset-md-4">
                    <div className="card">
                        <div className="card-header primary-background text-white text-center">
                            <span className="fa fa-user-plus fa-3x"></span>
                            <br />
                            <p>Login here</p>
                        </div>
                        <div className="card-body">

                            <div className="form-group">
                                <label for="exampleInputEmail1" style={style}>Email address</label>
                                <input onChange={(e) => {
                                    setEmail(e.target.value)
                                }} required type="email" className="form-control" placeholder="test@gmail.com" style={style} />
                            </div>
                            <div className="form-group">
                                <label for="exampleInputPassword1" style={style}>Password</label>
                                <input onChange={(e) => {
                                    setPassword(e.target.value)
                                }} required type="password" className="form-control" placeholder="******" style={style} />
                            </div>
                            <div className="container text-center">
                                <button onClick={onLogin} className="btn btn-primary" style={style}>Login</button>
                            </div>
                            <div>
                                <span>New User ?
                                <Link to="/register">
                                        <span>Register Here</span>
                                    </Link>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            { userSignin.loading && <div>waiting for result</div>}
        </div>
    )
}
const style = {
    margin: '8px'
}
export default LoginScreen