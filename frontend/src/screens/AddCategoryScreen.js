
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addCategory } from '../actions/categoryActions'


const AddCategoryScreen = (props) => {

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const dispatch = useDispatch()

    const addcategory = useSelector((store) => store.addCategory)
    const { loading, response, error } = addcategory

    const onAdd = () => {
        console.log('title', title)
        console.log('description', description)
        const id = null
        dispatch(addCategory(id,title,description))

    }

    useEffect(() => {
        //successfull addition of category
        if(response && (response.status == 'OK')){ 
            console.log(response.result)
            props.history.push('/admin_category')
            window.location.reload(false)
        }
         else if(error){
            //there is an error while making the api call
            console.log(error)
            alert('error while making API call')
        } 
    },[loading, error, response])

    return (
        <div>
            <div className="container">
                <div className="col-md-6 offset-md-3">
                    <div className="card">
                        <div className="card-header text-center primary-background text-white">
                            <br />
                                   Add a Category Here
                                </div>
                        <div className="card-body">
                                <div className="form-group">
                                    <label for="category_title">Title</label>
                                    <input onChange={(e) => {
                                    setTitle(e.target.value)
                                }} type="text" className="form-control" placeholder="Enter Title" />
                                </div>
                                <div className="form-group">
                                    <label for="category_description">Description</label>
                                    <textarea onChange={(e) => {
                                    setDescription(e.target.value)
                                }} className="form-control" rows="5" placeholder="Enter Description" ></textarea>
                                </div>
                                <br />
                                <button onClick={onAdd} type="submit" className="btn btn-primary" style={{ 'margin-top': '20px' }}>Add Category</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const style = {
    margin: '8px'
}

export default AddCategoryScreen