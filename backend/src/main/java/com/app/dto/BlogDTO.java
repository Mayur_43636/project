package com.app.dto;

import java.time.LocalDate;
import java.util.Arrays;

import com.app.pojos.Status;

public class BlogDTO {
	private int id;
	private String title;
	private String category;
	private String description;
	private String content;
	private String photo;
	private LocalDate date;
	private Status status;
	
	public BlogDTO() {
		System.out.println("in ctor of : "+getClass().getName());
	}

	public BlogDTO(int id, String title, String category,  String description, String content, String photo, LocalDate date,
			Status status) {
		super();
		this.id = id;
		this.title = title;
		this.category = category;
		this.description = description;
		this.content = content;
		this.photo = photo;
		this.date = date;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "BlogDTO [id=" + id + ", title=" + title + ", category=" + category + ", description=" + description + ", content=" + content
				+ ", photo=" + photo + ", date=" + date + ", status=" + status + "]";
	}
}
