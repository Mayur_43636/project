import { useEffect, useLayoutEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { addNews } from "../actions/newsActions"

const UpdateNewsScreen = (props) => {
    const news = props.location.state
    const [id, setId] = useState(news.id)
    const [title, setTitle] = useState(news.title)
    const [description, setDescription] = useState(news.description)
    const [date, setDate] = useState(news.date)

    const addnews = useSelector((store) => store.addNews)
    const { loading, response, error } = addnews

    const dispatch = useDispatch() 

    const onUpdate = () => {
        console.log('id', id)
        console.log('title', title)
        console.log('description', description)
        console.log('date', date)
        dispatch(addNews(id,title,description,date))
    }

    //console.log(props.location.state)

    useEffect(() => {
        //successfull updation of news
        if(response && (response.status == 'OK')){ 
            console.log(response.result)
            props.history.push('/admin_news')
            window.location.reload(false)
        }
         else if(error){
            //there is an error while making the api call
            console.log(error)
            alert('error while making API call')
        } 
    },[loading, error, response])

    return (
        <div>

            <div className="container">
                <div className="col-md-6 offset-md-3">
                    <div className="card">
                        <div className="card-header text-center primary-background text-white">
                            <br />
                                   Update a News Here
                                </div>
                        <div className="card-body">
                            <div className="form-group">
                                <label for="news-title">Title</label>
                                <input onChange={(e) => {
                                    setTitle(e.target.value)
                                }} type="text" className="form-control" value={title} />
                            </div>
                            <div className="form-group">
                                <label for="news-description">Description</label>
                                <textarea onChange={(e) => {
                                    setDescription(e.target.value)
                                }} className="form-control" rows="5" value={description}></textarea>
                            </div>
                            <div className="form-group">
                                <label for="news-date">Date</label>
                                <input onChange={(e) => {
                                    setDate(e.target.value)
                                }} type="date" className="form-control" value={date} />
                            </div>
                            <button onClick={onUpdate} type="submit" className="btn btn-primary" style={{ 'margin-top': '20px' }}>Update News</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const style = {
    margin: '8px'
}

export default UpdateNewsScreen