import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { getUserDetails, updateProfile } from "../actions/userActions"


const EditProfileScreen = (props) => {
    //const user = response
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [gender, setGender] = useState('')
    const [about, setAbout] = useState('')
    const [photo, setPhoto] = useState('')


    const getUserdetails = useSelector((store) => store.getUser)
    const { loading, response, error } = getUserdetails
    const update = useSelector((store) => store.updateProfile)

    useEffect(() => {
        dispatch(getUserDetails())
    }, [])


    useEffect(() => {
        if(response){
            const user = response.result
            setName(user.name)
            setEmail(user.email)
            setPassword(user.password)
            setAbout(user.about)
            setPhoto(user.photo)
            setGender(user.gender)
        }
    },[loading,response,error])

    const dispatch = useDispatch()

    const onFileAdd = (e) => {
        console.log(e)
        setPhoto(e.target.files[0])
    }

    const onUpdate = () => {
        const formdata = new FormData()
        formdata.append('imageFile', photo)
        formdata.append('name',name)
        formdata.append('email',email)
        formdata.append('password',password)
        formdata.append('gender',gender)
        formdata.append('about',about)
        dispatch(updateProfile(formdata))
        //console.log('name', name)
        //console.log('email', email)
        //console.log('password', password)
        //console.log('gender', gender)
        //console.log('about', about)
        //console.log('photo', photo)
    }

    const imageUrl = "http://localhost:8080/user/image/";

    return (
        <div className="container" >
            <div className="row">
                <div className="col-md-4 offset-md-4">
                    <div className="card">
                        <div className="d-flex justify-content-between p-2 outline"> <img src={imageUrl+photo} class="rounded" width="130" />
                            <div className="d-flex flex-column mr-1">
                                <div className="d-flex flex-row">
                                    <input onChange={onFileAdd} type="file" class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div className="card-body">
                            <div className="form-group">
                                <label for="exampleInputPassword1">Name</label>
                                <input onChange={(e) => {
                                    setName(e.target.value)
                                }} className="form-control" aria-describedby="name" value={name} readOnly />
                            </div>
                            <br />
                            <div className="form-group">
                                <label for="email">Email</label>
                                <input onChange={(e) => {
                                    setEmail(e.target.value)
                                }} type="text" className="form-control" value={email} readOnly/><br />
                            </div>
                            <br />
                            <div className="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input onChange={(e) => {
                                    setPassword(e.target.value)
                                }} type="password" className="form-control" value={password} />
                            </div>
                            <br />
                            <div className="form-group">
                                <label for="birth date">Gender</label> <br /><br />
                                <input onChange={(e) => {
                                    setGender(e.target.value)
                                }} type="radio" id="gender" name="gender" value="Male" /> &ensp; Male &ensp;
                                    <input onChange={(e) => {
                                    setGender(e.target.value)
                                }} type="radio" id="gender" name="gender" value="Female" /> &ensp;Female  &ensp;
                                </div>
                            <br />
                            <div className="form-group">
                                <label for="description">About</label>
                                <textarea onChange={(e) => {
                                    setAbout(e.target.value)
                                }} class="form-control" rows="3" value={about}></textarea >
                            </div>
                            <br />
                            <div className="container text-center">
                                <button onClick={onUpdate} className="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const style = {
    margin: '8px'
}

export default EditProfileScreen