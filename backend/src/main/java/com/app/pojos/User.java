package com.app.pojos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class User extends BaseEntity {

	@Column(length = 20,unique = true)
	private String name;
	@Column(length = 30,unique = true)
	@Email
	private String email;
	@Column(length = 20,nullable = false)
	private String password;
	@Column(length = 255)
	private String photo;
	@Column(length = 20)
	private String gender;
	@Column(length = 255)
	private String about;
	@Enumerated(EnumType.STRING)
	private Role role;
	
	@JsonIgnore
	@OneToMany(mappedBy = "users",cascade = CascadeType.ALL,orphanRemoval =true)
	private List<Blog> blogs = new ArrayList<Blog>();
	
	public User() {
		System.out.println("in ctor of : "+getClass().getName());
	}
	
	public User(String name, String email, String password, String photo, String gender, String about,
			Role role) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.photo = photo;
		this.gender = gender;
		this.about = about;
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Blog> getBlogs() {
		return blogs;
	}

	public void setBlogs(List<Blog> blogs) {
		this.blogs = blogs;
	}

	public void addBlog(Blog b) {
		blogs.add(b);
		b.setUser(this);
	}
	public void removeBlog(Blog b) {
		blogs.remove(b);
		b.setUser(null);
	}
	
	@Override
	public String toString() {
		return "User [id=" + getId() + ", name=" + name + ", email=" + email + ", password=" + password + ", photo=" + photo
				+ ", gender=" + gender + ", about=" + about + ", role=" + role + "]";
	}
}
