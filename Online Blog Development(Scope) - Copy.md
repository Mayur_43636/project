﻿### Online Blog Development


## Group Name: D10
* Member 1 - Snehal Khuspe         43315
* Member 2 - Neha Chavan           43358
* Member 3 - Sandesh Malvankar     43483
* Member 4 - Mayur Pawar           43636

## Roles:
* User
    * Admin
    * Blogger/Member
    * visitor


# User:
* Signup/Register
* Login
* Edit/Manage profile
* Manage Comment:
    * Add
    * Delete 
    * Replay

## Blogger/Member
* Manage Blog:
    * Create/Publish Post
    * Edit Post
    * Delete Post
* Categories - view

## Admin:
* Manage Categories: 
    * View
    * Edit/Add
    * Delete
* Manage all User Blog:
    * Verify blog 
    * View Published Post
    * Delete Post

## Visitor:
* Categories - view 

## Blog/Post
* Title
* Contents
* Copyright/timestamp
* Like post
* add comment
* view previous comments

## Comments 
* name
* comment -description

## Home Page Contents
* Home
* News
* Categories
* Skill test
* About us
* Contact us
* Log in
* Register
* Latest blogs/post