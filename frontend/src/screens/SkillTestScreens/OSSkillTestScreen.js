import React, { useState } from 'react';

const OSSkillTest = () => {
    const questions = [
        {
            questionText: 'What else is a command interpreter called?',
            answerOptions: [
                { answerText: 'prompt', isCorrect: false },
                { answerText: 'kernel', isCorrect: false },
                { answerText: 'shell', isCorrect: true },
                { answerText: 'command', isCorrect: false },
            ],
        },
        {
            questionText: 'What is the full name of FAT?',
            answerOptions: [
                { answerText: 'File attribute table', isCorrect: false },
                { answerText: 'File allocation table', isCorrect: true },
                { answerText: 'Font attribute table', isCorrect: false },
                { answerText: 'Format allocation table', isCorrect: false },
            ],
        },
        {
            questionText: 'BIOS is used?',
            answerOptions: [
                { answerText: 'By operating system', isCorrect: true },
                { answerText: 'By compiler', isCorrect: false },
                { answerText: 'By interpreter', isCorrect: false },
                { answerText: 'By application software', isCorrect: false },
            ],
        },
        {
            questionText: 'What is the mean of the Booting in the operating system?',
            answerOptions: [
                { answerText: 'Restarting computer', isCorrect: true },
                { answerText: 'Install the program', isCorrect: false },
                { answerText: 'To scan', isCorrect: false },
                { answerText: 'To turn off', isCorrect: false },
            ],
        },
        {
            questionText: 'When does page fault occur?',
            answerOptions: [
                { answerText: 'The page is present in memory.', isCorrect: false },
                { answerText: 'The deadlock occurs.', isCorrect: false },
                { answerText: 'The page does not present in memory.', isCorrect: true },
                { answerText: 'The buffering occurs.', isCorrect: false },
            ],
        },
    ];

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [showScore, setShowScore] = useState(false);
    const [score, setScore] = useState(0);

    const handleAnswerOptionClick = (isCorrect) => {
        if (isCorrect) {
            setScore(score + 1);
        }

        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < questions.length) {
            setCurrentQuestion(nextQuestion);
        } else {
            setShowScore(true);
        }
    };
    return (
        <>
            <div className="container" style={{ 'margin-top': '40px' }}>
                <div className='skill'>
                    {showScore ? (
                        <div className='score-section'>
                            You scored {score} out of {questions.length}
                        </div>
                    ) : (
                        <>
                            <div className='question-section'>
                                <div className='question-count'>
                                    <span>Question {currentQuestion + 1}</span>/{questions.length}
                                </div>
                                <div className='question-text'>{questions[currentQuestion].questionText}</div>
                                <div className='answer-section'>
                                    {questions[currentQuestion].answerOptions.map((answerOption) => (
                                        <button onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
                                    ))}
                                </div>
                            </div>

                        </>
                    )}
                </div>
            </div>
        </>
    );
}

export default OSSkillTest