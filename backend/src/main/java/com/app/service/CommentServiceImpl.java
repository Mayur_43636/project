package com.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.BlogRepository;
import com.app.dao.CommentRepository;
import com.app.dto.CommentDTO;
import com.app.pojos.Blog;
import com.app.pojos.Comment;
import com.app.pojos.User;

@Service
@Transactional
public class CommentServiceImpl implements ICommentService {
	
	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private BlogRepository blogRepo;

	@Override
	public String addComment(int id, Comment comment) {
		Optional<Blog> blog =blogRepo.findById(id);
		blog.get().getComments().add(comment);
		comment.setBlogs(blog.get());
		return "Comment added";
	}
	@Override
	public List<CommentDTO> getCommentList(int id) {
		List<CommentDTO> listDTO = new ArrayList<CommentDTO>();
		System.out.println("in service");
		List<Comment> list = new ArrayList<Comment>();
		list =  commentRepository.getListComment(id);
		for (Comment comment : list) {
			listDTO.add(new CommentDTO(comment.getId(), comment.getName(), comment.getDescription()));
		}
		return listDTO;
	}
}
