package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.CategoryDTO;
import com.app.dto.ResponseDTO;
import com.app.service.ICategoryService;

@RestController
@RequestMapping("/category")
@CrossOrigin
public class CategoryController {
	//dependency
		@Autowired
		private ICategoryService categoryService;
		
		@PostMapping("/add")
		private ResponseDTO<?> addCategoryDetails(@RequestBody CategoryDTO categoryDTO){
			System.out.println("in add category details ");
			return new ResponseDTO<>(HttpStatus.OK,"Category details added",categoryService.addCategoryDetails(categoryDTO));
		}
		
		@GetMapping()
		private ResponseDTO<?> getCategoryDetails(){
			System.out.println("in get category details ");
			return new ResponseDTO<>(HttpStatus.OK,"category details",categoryService.getCategorydetails());
		}
		
		@DeleteMapping("/delete/{id}")
		private ResponseDTO<?> deleteCategoryDetails(@PathVariable int id){
			System.out.println("in delete category details");
			categoryService.deleteCategory(id);
			return new ResponseDTO<>(HttpStatus.OK, "category deleted", "");
		}
}
