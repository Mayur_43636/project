import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { getBlogById } from "../actions/blogActions"
import { addComments, getComments } from "../actions/commentActions"

const DisplayBlogScreen = (props) => {
    const [flag,setFlag] = useState(false)
    const name = sessionStorage.getItem('user_name')
    const [description,setDescription] = useState('')
    const getBlog = useSelector((store) => store.getblog)
    const { loading, response, error } = getBlog
    const getcomments = useSelector((store) => store.getComments)
    const blog_id = sessionStorage.getItem('blog_id')
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getBlogById(blog_id))
        dispatch(getComments(blog_id))
    }, [])

    useEffect(() => {
       
    }, [loading, response, error])

    useEffect(() => {
       
    }, [getcomments.loading])


    console.log(response)

    
    const onPost = () => {
        console.log(description)
        console.log(name)
        dispatch(addComments(sessionStorage.getItem('blog_id'),name,description))
        setDescription('')
        setFlag(true)
        
    }
    if(flag){
        setFlag(false)
        dispatch(getComments(blog_id))
    }
    const onCancel = () => {
        console.log('click')
        setDescription('')
    }

    const imageUrl = "http://localhost:8080/blog/image/"
    const imageProfile = "http://localhost:8080/user/image/"
    return (
        <div class="container mt-5">
            { response &&
                <form class="form-style-9">
                    <article>
                        {/*title */}
                        <h2><a href="singlepost.html">{response.result.title}</a></h2>

                        <div class="row">
                            <div class="group1 col-sm-6 col-md-6">
                            </div>
                            <div class="group2 col-sm-6 col-md-6">
                                <span class="glyphicon glyphicon-pencil"></span> <a href="singlepost.html#comments">20 Comments</a>
                                <span class="glyphicon glyphicon-time"></span> <a href="singlepost.html#comments"> Date : {response.result.date}</a>
                            </div>
                        </div>

                        <hr />
                        <center>
                            <img src={imageUrl + response.result.photo} class="img-responsive" />
                        </center>
                        <br />
                        {/* description*/}
                        <p class="lead">
                            {response.result.description}
                        </p>

                        {/*content*/}
                        <p>
                            {response.result.content}
                        </p>
                        <hr />

                    </article>
                    <div class="d-flex justify-content-center row">
                        <div class="col-md-8">
                            <div class="d-flex flex-column comment-section">
                                <p>Comments : </p>
                                {getcomments.response && getcomments.response.result.length > 0 &&
                                    getcomments.response.result.map((comment) => {
                                        return (
                                            <div class="bg-white p-2">
                                                <div class="d-flex flex-row user-info"><img class="rounded-circle" src="https://i.imgur.com/RpzrMR2.jpg" width="40" />
                                                    <div class="d-flex flex-column justify-content-start ml-2"><span class="d-block font-weight-bold name">{comment.name}</span></div>
                                               </div>
                                                <div class="mt-2">
                                                    <p class="comment-text">{comment.description}</p>
                                                </div>
                                            </div>)
                                    })}
                                <div class="bg-white">
                                    <div class="d-flex flex-row fs-12">
                                        <div class="like p-2 cursor"><i class="fa fa-thumbs-o-up"></i><span class="ml-1">Like</span></div>
                                        <div class="like p-2 cursor"><i class="fa fa-commenting-o"></i><span class="ml-1">Comment</span></div>
                                        <div class="like p-2 cursor"><i class="fa fa-share"></i><span class="ml-1">Share</span></div>
                                    </div>
                                </div>
                                {  name && 
                                <div class="bg-light p-2">
                                    <div class="d-flex flex-row align-items-start">{/*<img class="rounded-circle" src={imageProfile + response.result.user.photo}> width="40" />*/}<textarea onChange={(e) => {setDescription(e.target.value)}} value={description} class="form-control ml-1 shadow-none textarea"></textarea></div>
                                    <div class="mt-2 text-right"><button onClick={onPost} class="btn btn-primary btn-sm shadow-none" type="button">Post comment</button><button onClick={onCancel} class="btn btn-outline-primary btn-sm ml-1 shadow-none" type="button">Cancel</button></div>
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                </form>}
        </div>
    )
}

export default DisplayBlogScreen