package com.app.dto;

import java.time.LocalDate;

public class NewsDTO {
	private int id;
	private String title;
	private String description;
	private LocalDate date;
	
	
	public NewsDTO() {
		System.out.println("in ctor of : "+getClass().getName());
	}


	public NewsDTO(String title, String description, LocalDate date) {
		super();
		this.title = title;
		this.description = description;
		this.date = date;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public LocalDate getDate() {
		return date;
	}


	public void setDate(LocalDate date) {
		this.date = date;
	}


	@Override
	public String toString() {
		return "NewsDTO [title=" + title + ", description=" + description + ", date=" + date + "]";
	}
	
	
	
	
}
