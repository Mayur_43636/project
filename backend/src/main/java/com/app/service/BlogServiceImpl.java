package com.app.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.BlogRepository;
import com.app.dao.UserRepository;
import com.app.dto.BlogDTO;
import com.app.pojos.Blog;
import com.app.pojos.Comment;
import com.app.pojos.Status;
import com.app.pojos.User;

@Service
@Transactional
public class BlogServiceImpl implements IBlogService {

	@Autowired
	private BlogRepository blogRepository;
	@Autowired
	private UserRepository userRepository;

	@Override
	public List<BlogDTO> getBlogContent() {
		System.out.println("Get Blog Content");
		// blogRepository.findAll().forEach(System.out::println);
		List<Blog> list = blogRepository.findAll();

		for (Blog blog : list) {
			System.out.println("here");
			System.out.println(blog.toString());
		}
		List<BlogDTO> listDTO = new ArrayList<BlogDTO>();
		for (Blog blog : list) {
			listDTO.add(new BlogDTO(blog.getId(), blog.getTitle(), blog.getCategory(), blog.getDescription(),
					blog.getContent(), blog.getPhoto(), blog.getDate(), blog.getStatus()));
		}
		return listDTO;
	}

	@Override
	public void deleteBlog(int id) {
		blogRepository.deleteById(id);
	}

	@Override
	public List<BlogDTO> getSubmittedBlogs() {
		System.out.println("in get submitted blogs");
		List<Blog> list = blogRepository.getSubmittedBlogs();
		List<BlogDTO> listDTO = new ArrayList<BlogDTO>();
		for (Blog blog : list) {
			System.out.println(blog.toString());
			listDTO.add(new BlogDTO(blog.getId(), blog.getTitle(), blog.getCategory(), blog.getDescription(),
					blog.getContent(), blog.getPhoto(), blog.getDate(), blog.getStatus()));
		}
		return listDTO;
	}

	@Override
	public String changeStatus(int id, String status) {
		// get persistent blog pojo
		Blog blog = blogRepository.findById(id).get();
		if (Status.APPROVED.toString().equals(status))
			blog.setStatus(Status.APPROVED);
		else if (Status.REJECTED.toString().equals(status))
			blog.setStatus(Status.REJECTED);
		return status;
	}

	@Override
	public String blogAdd(int id, Blog blog) {
		Optional<User> user = userRepository.findById(id);
		user.get().getBlogs().add(blog);
		blog.setUser(user.get());
		return "Blog added";
	}

	@Override
	public List<BlogDTO> getApprovedBlogs() {
		System.out.println("in get approved blogs");
		List<Blog> list = blogRepository.getApprovedBlogs();
		List<BlogDTO> listDTO = new ArrayList<BlogDTO>();
		for (Blog blog : list) {
			listDTO.add(new BlogDTO(blog.getId(), blog.getTitle(), blog.getCategory(), blog.getDescription(),
					blog.getContent(), blog.getPhoto(), blog.getDate(), blog.getStatus()));
		}
		return listDTO;
	}

	@Override
	public List<BlogDTO> getBlogsByCategory(String category) {
		System.out.println("in get blogs by category");
		List<Blog> list = blogRepository.getBlogsByCategory(category);
		List<BlogDTO> listDTO = new ArrayList<BlogDTO>();
		for (Blog blog : list) {
			listDTO.add(new BlogDTO(blog.getId(), blog.getTitle(), blog.getCategory(), blog.getDescription(),
					blog.getContent(), blog.getPhoto(), blog.getDate(), blog.getStatus()));
		}
		return listDTO;
	}

	@Override
	public BlogDTO getBlogById(int id) {
		System.out.println("in get blog by id");
		Optional<Blog> blog = blogRepository.findById(id);
		BlogDTO blogDTO = new BlogDTO(blog.get().getId(), blog.get().getTitle(), blog.get().getCategory(),
				blog.get().getDescription(), blog.get().getContent(), blog.get().getPhoto(), blog.get().getDate(),
				blog.get().getStatus());
		return blogDTO;
	}

	@Override
	public List<Comment> getCommentList(int id) {
		System.out.println("here");
		Optional<Blog> b = blogRepository.findById(id);
		return b.get().getComments();
	}

	@Override
	public List<BlogDTO> getPublishedBlogByUser(int id) {
		System.out.println("in blog service");
		List<Blog> list = blogRepository.getPublishedBlogByUser(id);
		List<BlogDTO> listDTO = new ArrayList<BlogDTO>();
		for (Blog blog : list) {
			listDTO.add(new BlogDTO(blog.getId(), blog.getTitle(), blog.getCategory(), blog.getDescription(),
					blog.getContent(), blog.getPhoto(), blog.getDate(), blog.getStatus()));
		}
		return listDTO;
	}

}
