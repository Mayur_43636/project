import { COMMENT_ADD_FAIL, 
    COMMENT_ADD_REQUEST, 
    COMMENT_ADD_SUCCESS, 
    COMMENT_FETCH_FAIL, 
    COMMENT_FETCH_REQUEST, 
    COMMENT_FETCH_SUCCESS } from "../constants/commentConstants"
import axios from 'axios'

export const getComments = (id) => {
    return (dispatch) => {
      dispatch({
        type: COMMENT_FETCH_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const url = 'http://localhost:8080/comment/list/'+id
      axios
        .get(url, header)
        .then((response) => {
          dispatch({
            type: COMMENT_FETCH_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: COMMENT_FETCH_FAIL,
            payload: error,
          })
        })
    }
  }

  export const addComments = (id,name,description) => {
    return (dispatch) => {
      dispatch({
        type: COMMENT_ADD_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
      const body = {
        name, description, 
      }
      const url = 'http://localhost:8080/comment/add/'+id
      axios
        .post(url, body, header)
        .then((response) => {
          dispatch({
            type: COMMENT_ADD_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: COMMENT_ADD_FAIL,
            payload: error,
          })
        })
    }
  }