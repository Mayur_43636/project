package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Webinar;

public interface WebinarRepository extends JpaRepository<Webinar, Integer> {

}
