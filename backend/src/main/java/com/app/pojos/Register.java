package com.app.pojos;

import javax.persistence.*;
import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "register")
public class Register extends BaseEntity {
	@Column(length = 20)
	private String name;
	@Column(length = 30)
	@Email
	private String email;
	@Column(length = 15)
	private String mobile;
	
	public Register() {
		System.out.println("in ctor of : "+getClass().getName());
	}

	public Register(String name, String email, String mobile) {
		super();
		this.name = name;
		this.email = email;
		this.mobile = mobile;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Override
	public String toString() {
		return "Register [id=" + getId() + ", name=" + name + ", email=" + email + ", mobile=" + mobile + "]";
	}
}
