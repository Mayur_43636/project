import {
  BLOG_ADD_REQUEST,
  BLOG_ADD_SUCCESS,
  BLOG_ADD_FAIL,
  BLOG_ADD_RESET,
  BLOG_FETCH_REQUEST,
  BLOG_FETCH_SUCCESS,
  BLOG_FETCH_FAIL,
  BLOG_FETCH_RESET,
  BLOG_STATUS_RESET,
  BLOG_STATUS_SUCCESS,
  BLOG_STATUS_FAIL,
  BLOG_STATUS_REQUEST,
  DELETE_BLOG_REQUEST,
  DELETE_BLOG_SUCCESS,
  DELETE_BLOG_FAILURE
} from '../constants/blogConstants'

export const addblogReducer = (state = {}, action) => {
  switch (action.type) {
    case BLOG_ADD_REQUEST:
      return { loading: true }
    case BLOG_ADD_SUCCESS:
      return { loading: false, response: action.payload }
    case BLOG_ADD_FAIL:
      return { loading: false, error: action.payload }
    case BLOG_ADD_RESET:
      return {}
    default:
      return state
  }
}

export const fetchblogReducer = (state = {}, action) => {
  switch (action.type) {
    case BLOG_FETCH_REQUEST:
      return { loading: true }
    case BLOG_FETCH_SUCCESS:
      return { loading: false, response: action.payload }
    case BLOG_FETCH_FAIL:
      return { loading: false, error: action.payload }
    case BLOG_FETCH_RESET:
      return {}
    default:
      return state
  }
}

export const statusblogReducer = (state = {}, action) => {
  switch (action.type) {
    case BLOG_STATUS_REQUEST:
      return { loading: true }
    case BLOG_STATUS_SUCCESS:
      return { loading: false, response: action.payload }
    case BLOG_STATUS_FAIL:
      return { loading: false, error: action.payload }
    case BLOG_STATUS_RESET:
      return {}
    default:
      return state
  }
}

export const deleteblogReducer = (state = {}, action) => {
  switch (action.type) {
    case DELETE_BLOG_REQUEST:
      return { loading: true }
    case DELETE_BLOG_SUCCESS:
      return { loading: false, response: action.payload }
    case DELETE_BLOG_FAILURE:
      return { loading: false, error: action.payload }
    default:
      return state
  }
}