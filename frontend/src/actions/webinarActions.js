import { WEBINAR_ADD_FAIL, 
    WEBINAR_ADD_REQUEST, 
    WEBINAR_ADD_SUCCESS, 
    WEBINAR_DELETE_FAIL, 
    WEBINAR_DELETE_REQUEST, 
    WEBINAR_DELETE_SUCCESS, 
    WEBINAR_FETCH_FAIL, 
    WEBINAR_FETCH_REQUEST, 
    WEBINAR_FETCH_SUCCESS } from "../constants/webinarConstants"
import axios from 'axios'

export const getWebinars = () => {
    return (dispatch) => {
      dispatch({
        type: WEBINAR_FETCH_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const url = 'http://localhost:8080/webinar'
      axios
        .get(url, header)
        .then((response) => {
          dispatch({
            type: WEBINAR_FETCH_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: WEBINAR_FETCH_FAIL,
            payload: error,
          })
        })
    }
  }

  //add webinar action
  export const addWebinar = (id,title, description, date) => {
    return (dispatch) => {
      dispatch({
        type: WEBINAR_ADD_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const body = {
        id,
        title,
        description,
        date,
      }
      const url = 'http://localhost:8080/webinar/add'
      axios
        .post(url, body, header)
        .then((response) => {
          dispatch({
            type: WEBINAR_ADD_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: WEBINAR_ADD_FAIL,
            payload: error,
          })
        })
    }
  }

  //add webinar action
  export const deleteWebinar = (id) => {
    return (dispatch) => {
      dispatch({
        type: WEBINAR_DELETE_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }

      const url = 'http://localhost:8080/webinar/delete/'+id
      axios
        .delete(url, header)
        .then((response) => {
          dispatch({
            type: WEBINAR_DELETE_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: WEBINAR_DELETE_FAIL,
            payload: error,
          })
        })
    }
  }

  