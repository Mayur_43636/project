import { Link } from "react-router-dom"

import  image1 from '../images/travel.jpg';

const ContactUsScreen = () => {
    return (
    
        <div >
            <h1>Contact Us</h1>
            <h5>Feel free to drop us a line below....!</h5>

        <div className="container-fluid" >
            <div className="row" >
                <div className="col-md-4 offset-md-4">
                    <div className="card " >

                        <div className="card-body" >
                            <form >
                                <div className="form-group">
                                   
                                    <input name="Name" required type="text" className="form-control" id="name" aria-describedby="name" placeholder="Enter your name" />
                                </div>
                                <br />
                                <div className="form-group">
                                  
                                    <input name="email" required type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Name@gmail.com" />
                                </div>
                                <br />
                                <div className="form-group">
                                
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="2"  placeholder="Enter Message"></textarea >

                                </div>
                                <br />

                                <div className="container text-center ">
                                    
                                    <button type="Send" className="btn btn-primary">Send</button>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>

            </div>

            <br />

            <div className="row ">
            <div className="col-md-4 offset-md-4"> 
            <div className="card">
           <div className="container">
        <h4 >Address</h4>
        <p>Avnish regency,near nawale bridge,vadgaon,Pune</p>
        <h4>Email</h4>
        <p>Admin@gmail.com</p>
        <h4>Phone</h4>
        <p>+91-9865475233</p>


        </div>
        </div>
            
            </div> 
               
               
                </div>
          </div>
       </div>
    )

}

export default ContactUsScreen