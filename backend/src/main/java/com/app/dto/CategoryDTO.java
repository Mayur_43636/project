package com.app.dto;

public class CategoryDTO {
	
	private int id;
	private String title;
	private String description;

	public CategoryDTO() {
		System.out.println("in ctor of : " + getClass().getName());
	}

	public CategoryDTO(int id, String title, String description) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "CategoryDTO [id=" + id + ", title=" + title + ", description=" + description + "]";
	}

	
}
