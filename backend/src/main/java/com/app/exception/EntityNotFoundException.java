package com.app.exception;

import com.app.pojos.Role;

public class EntityNotFoundException {

	private Integer id;
	
	private Role role;

	
	public EntityNotFoundException(Integer id, Role role) {
	
		this.id = id;
		this.role = role;
	}


	public Integer getId() {
		return id;
	}
	
	
	public String getMessage()
	{
		return role + "with Id" + id +"not found";
	}
	
	
	
}
