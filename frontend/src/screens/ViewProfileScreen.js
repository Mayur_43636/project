import { Link } from "react-router-dom"
import Header from "../components/Header"
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from "react"
import { getUserDetails } from "../actions/userActions"


const ViewProfileScreen = () => {

    const dispatch = useDispatch()
    const user = useSelector((store) => store.getUser)
    const { loading, response, error } = user

    //call once when page loaded successfully
    useEffect(() => {
        dispatch(getUserDetails(sessionStorage.getItem('token')))
    }, [])

    useEffect(() => { }, [loading, response, error])
    //console.log(window.location.pathname)
    console.log('hiii', response)

    return (
        <div className="container">
            <div className="row">
                <Header title="User Profile" />
               
                {response &&

                <div className="news col-md-3">
                    <div className="id">id: {response.result.id}</div>
                    <div className="name">name: {response.result.name}</div>
                    <div className="email">email:{response.result.email}</div>
                    <div className="password">password: {response.result.password}</div>
                    <div className="gender">gender:{response.result.gender} </div>
                    <div className="about"> about:{response.result.about}</div>
                    {/* <div className="photo"> photo:{response.result.photo}</div> */}
                </div>

}
                
            </div>

            <div>
                <Link to={{
                    pathname: "/edit_profile",
                    state: user
                    }}>
                    <button
                        className="btn btn-sm btn-success btn-add-to-cart" style={buttonStyle}>
                        Update
                                    </button>
                </Link>

            </div>


        </div>

    )
}

const buttonStyle = {
    margin: '10px'
}

export default ViewProfileScreen
