import { useEffect, useLayoutEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { addWebinar } from "../actions/webinarActions"


const UpdateWebinarScreen = (props) => {
    const webinar = props.location.state
    const [id, setId] = useState(webinar.id)
    const [title, setTitle] = useState(webinar.title)
    const [description, setDescription] = useState(webinar.description)
    const [date, setDate] = useState(webinar.date)

    const addwebinar = useSelector((store) => store.addWebinar)
    const { loading, response, error } = addwebinar

    const dispatch = useDispatch() 

    const onUpdate = () => {
        console.log('id', id)
        console.log('title', title)
        console.log('description', description)
        console.log('date', date)
        dispatch(addWebinar(id,title,description,date))
    }

    //console.log(props.location.state)

    useEffect(() => {
        //successfull updation of webinar
        if(response && (response.status == 'OK')){ 
            console.log(response.result)
            props.history.push('/admin_webinar')
            window.location.reload(false)
        }
         else if(error){
            //there is an error while making the api call
            console.log(error)
            alert('error while making API call')
        } 
    },[loading, error, response])

    return (
        <div>

            <div className="container">
                <div className="col-md-6 offset-md-3">
                    <div className="card">
                        <div className="card-header text-center primary-background text-white">
                            <br />
                                   Add a Webinar Here
                                </div>
                        <div className="card-body">
                            <div className="form-group">
                                <label for="webinar-title">Title</label>
                                <input onChange={(e) => {
                                    setTitle(e.target.value)
                                }} type="text" className="form-control" value={title} />
                            </div>
                            <div className="form-group">
                                <label for="webinar-description">Description</label>
                                <textarea onChange={(e) => {
                                    setDescription(e.target.value)
                                }} className="form-control" rows="5" value={description}></textarea>
                            </div>
                            <div className="form-group">
                                <label for="webinar-date">Date</label>
                                <input onChange={(e) => {
                                    setDate(e.target.value)
                                }} type="date" className="form-control" value={date} />
                            </div>
                            <button onClick={onUpdate} type="submit" className="btn btn-primary" style={{ 'margin-top': '20px' }}>Update Webinar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const style = {
    margin: '8px'
}

export default UpdateWebinarScreen