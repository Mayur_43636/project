import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { addblog } from "../actions/blogActions"
import { getCategory } from "../actions/categoryActions"

const CreateBlogScreen = (props) => {

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [content, setContent] = useState('')
    const [file, setFile] = useState('')
    const [category, setCategory] = useState('')

    const addBlog = useSelector((store) => store.addblog)
    const { loading, response, error } = addBlog

    const dispatch = useDispatch()
    //for category
    const categories = useSelector((store) => store.getCategory)
    useEffect(() => {
        dispatch(getCategory())
    }, [])

    useEffect(() => {
    }, [categories.loading, categories.response, categories.error])



    const onFileAdd = (e) => {
        console.log(e)
        setFile(e.target.files[0])
    }

    const onAdd = () => {
        const formdata = new FormData()
        formdata.append('imageFile', file)
        formdata.append('title', title)
        formdata.append('description', description)
        formdata.append('content', content)
        formdata.append('content', content)
        formdata.append('category', category)
       
        console.log('title', title)
        console.log('description', description)
        console.log('content', content)
        console.log('category', category)
        dispatch(addblog(formdata))
    }

    useEffect(() => {
        //successfull addition of blog
        if (response && (response.status == 'OK')) {
            console.log(response.result)
            //props.history.push('/')
            //window.location.reload(false)
            //document.location.href = '/'
        }
        else if (error) {
            //there is an error while making the api call
            console.log(error)
            alert('error while making API call')
        }
    }, [loading, error, response])

    return (
        <div>
            <div className="container" style={{ 'marginTop': '40px' }} >
                <div className="row">
                    <div className="col-md-4 offset-md-4">
                        <div className="card">
                            <div className="card-header primary-background text-white text-center">
                                <span className="fa fa-edit fa-3x"></span>
                                <br />
                                <p>Create Blog</p>
                            </div>
                            <div className="card-body">
                                <form >
                                    <div className="form-group">

                                        <input onChange={(e) => {
                                            setTitle(e.target.value)
                                        }} required type="text" className="form-control" aria-describedby="" placeholder="Enter Blog Title" />
                                    </div>
                                    <br />
                                    <div>
                                        <div className="form-group">
                                            <textarea onChange={(e) => {
                                                setDescription(e.target.value)
                                            }} required type="text" rows="2" className="form-control" placeholder="Enter description of Blog" />
                                        </div>
                                    </div>
                                    <br />
                                    <div className="form-control" style={{ 'textAlign': 'left' }}>
                                        <p>
                                            <label >Select Category </label> &ensp;
                                            <select id="myList" onChange={(e) => {
                                                                setCategory(e.target.value)
                                                            }}>
                                                {categories.response && categories.response.result.length > 0 &&
                                                    categories.response.result.map((category) => {
                                                        return (
                                                            <option value={category.title}>{category.title}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                        </p>
                                    </div>

                                    <br />
                                    <div>
                                        <div className="form-group">
                                            <textarea onChange={(e) => {
                                                setContent(e.target.value)
                                            }} required type="text" rows="4" className="form-control" placeholder="Enter Your Blog Content here" />
                                        </div>
                                    </div>

                                    <br />
                                    <div className="form-group">
                                        <br />
                                        <input onChange={onFileAdd} type="file" name="pic" />
                                    </div>
                                    <br />
                                    <div className="container text-center">
                                        <button onClick={onAdd} className="btn btn-primary">Post</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateBlogScreen