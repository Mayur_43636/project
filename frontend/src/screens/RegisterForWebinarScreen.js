import { Link } from "react-router-dom"

const RegisterForWebinarScreen = (props) => {

    const onRegister = () => {
        props.history.push('/webinar')
    } 
    return (
        <div>
            <div className="container">
                <div className="col-md-6 offset-md-3">
                    <div className="card">
                        <div className="card-header text-center primary-background text-white">
                            <br />
                                   Register for a Webinar Here
                                </div>
                        <div className="card-body">
                            <form id="register-webinar">
                                <div className="form-group">
                                    <label for="name" style={style}>Name</label>
                                    <input name="name" type="text" className="form-control" placeholder="Enter Name" style={style} />
                                </div>
                                <div className="form-group">
                                    <label for="exampleInputEmail1" style={style}>Email</label>
                                    <input type="email" className="form-control" placeholder="test@gmail.com" style={style} />
                                </div>
                                <div className="form-group">
                                    <label for="mobile" style={style}>Mobile</label>
                                    <input name="mobile" type="text" className="form-control" placeholder="Enter Mobile No." style={style} />
                                </div>
                                <button onClick={onRegister} type="submit" className="btn btn-primary" style={{ 'margin-top': '20px' }}>Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const style = {
                margin: '8px',
}


export default RegisterForWebinarScreen