import React, { useState } from 'react';

const SpringSkillTest = () => {
    const questions = [
        {
            questionText: 'Which of the following is correct assertion about spring?',
            answerOptions: [
                { answerText: 'Spring enables developers to develop enterprise-class applications using POJOs.', isCorrect: false },
                { answerText: 'Spring is organized in a modular fashion.', isCorrect: false },
                { answerText: ' Testing an application written with spring is simple because environment-dependent code is moved into this framework.', isCorrect: false },
                { answerText: 'All of above.', isCorrect: true },
            ],
        },
        {
            questionText: 'What is singleton scope?',
            answerOptions: [
                { answerText: 'This scopes the bean definition to a single instance per Spring IoC container.', isCorrect: true },
                { answerText: 'This scopes the bean definition to a single instance per HTTP Request.', isCorrect: false },
                { answerText: 'This scopes the bean definition to a single instance per HTTP Session.', isCorrect: false },
                { answerText: 'This scopes the bean definition to a single instance per HTTP Application/ Global session.', isCorrect: false },
            ],
        },
        {
            questionText: 'How can you inject Java Collection in Spring?',
            answerOptions: [
                { answerText: 'Using list, set, map or props tag.', isCorrect: true },
                { answerText: 'Using lit, set, map or collection tag.', isCorrect: false },
                { answerText: 'Using list, set, props or collection tag.', isCorrect: false },
                { answerText: 'Using list, collection, map or props tag.', isCorrect: false },
            ],
        },
        {
            questionText: 'Which are the different modes of autowiring?',
            answerOptions: [
                { answerText: 'prno, byName, byType, constructor, autodetectompt', isCorrect: true },
                { answerText: 'no, byName, byType, constructor, autocorrect', isCorrect: false },
                { answerText: 'byName, byContent, constructor, autodetect', isCorrect: false },
                { answerText: ' byName, byContent, setter, autodetect', isCorrect: false },
            ],
        },
        {
            questionText: 'Which of the following database is not supported using jdbcTemplate?',
            answerOptions: [
                { answerText: 'MySql', isCorrect: false },
                { answerText: 'PostgresSql', isCorrect: false },
                { answerText: 'NoSql', isCorrect: true },
                { answerText: 'Oracle', isCorrect: false },
            ],
        },
    ];

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [showScore, setShowScore] = useState(false);
    const [score, setScore] = useState(0);

    const handleAnswerOptionClick = (isCorrect) => {
        if (isCorrect) {
            setScore(score + 1);
        }

        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < questions.length) {
            setCurrentQuestion(nextQuestion);
        } else {
            setShowScore(true);
        }
    };
    return (
        <>
            <div className="container" style={{ 'margin-top': '40px' }}>
                <div className='skill'>
                    {showScore ? (
                        <div className='score-section'>
                            You scored {score} out of {questions.length}
                        </div>
                    ) : (
                        <>
                            <div className='question-section'>
                                <div className='question-count'>
                                    <span>Question {currentQuestion + 1}</span>/{questions.length}
                                </div>
                                <div className='question-text'>{questions[currentQuestion].questionText}</div>
                                <div className='answer-section'>
                                    {questions[currentQuestion].answerOptions.map((answerOption) => (
                                        <button onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
                                    ))}
                                </div>
                            </div>

                        </>
                    )}
                </div>
            </div>
        </>
    );
}

export default SpringSkillTest