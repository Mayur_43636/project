package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ResponseDTO;
import com.app.dto.WebinarDTO;
import com.app.service.IWebinarService;

@RestController
@RequestMapping("/webinar")
@CrossOrigin
public class WebinarController {

	@Autowired
	private IWebinarService webService;
	
	@PostMapping("/add")
	private ResponseDTO<?> addWebinarDetails(@RequestBody WebinarDTO webDTO){
		System.out.println("in add webinar details ");
		return new ResponseDTO<>(HttpStatus.OK,"webinar details added",webService.addWebinarDetails(webDTO));
	}
	
	@GetMapping()
	private ResponseDTO<?> getWebinarDetails(){
		System.out.println("in get web details ");
		return new ResponseDTO<>(HttpStatus.OK,"webinar details",webService.getWebinardetails());
	}
	
	@DeleteMapping("/delete/{id}")
	private ResponseDTO<?> deleteWebinarDetails(@PathVariable int id){
		System.out.println("in delete web details");
		webService.deleteWebinar(id);
		return new ResponseDTO<>(HttpStatus.OK, "webinar deleted", "");
	}

}
