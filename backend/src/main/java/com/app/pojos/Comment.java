package com.app.pojos;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "comment")
public class Comment extends BaseEntity {
	
	@Column(length = 30)
	private String name;
	@Column(length = 255)
	private String description;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "blog_id",nullable=false)
    private Blog blogs;

	public Comment() {
		System.out.println("in ctor of : " + getClass().getName());
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public Comment(String description) {
		super();
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Blog getBlogs() {
		return blogs;
	}

	public void setBlogs(Blog blogs) {
		this.blogs = blogs;
	}

	@Override
	public String toString() {
		return "Comment [id=" + getId() + ", description=" + description + "]";
	}
}
