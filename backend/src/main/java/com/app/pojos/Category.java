package com.app.pojos;


import javax.persistence.*;



@Entity
@Table(name = "categories")
public class Category extends BaseEntity {

	@Column(length = 20)
	private String title;
	@Column(length = 255)
	private String description;
	
	public Category() {
		System.out.println("in ctor of : "+getClass().getName());
	}

	public Category(String title, String description) {
		super();
		this.title = title;
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Category [id=" + getId() + ", title=" + title + ", description=" + description + "]";
	}
}
