import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { addNews} from '../actions/newsActions'

const AddNewsScreen = (props) => {

    const [title,setTitle] = useState('')
    const [description,setDescription] = useState('')
    const [date,setDate] = useState('')

    const addnews = useSelector((store) => store.addNews)
    const { loading, response, error } = addnews

    const dispatch = useDispatch() 

    const onAdd = () => {
        console.log('title', title)
        console.log('description', description)
        console.log('date', date)
        const id = null
        dispatch(addNews(id,title,description,date))

    }

    useEffect(() => {
        //successfull addition of news
        if(response && (response.status == 'OK')){ 
            console.log(response.result)
            props.history.push('/admin_news')
            window.location.reload(false)
        }
         else if(error){
            //there is an error while making the api call
            console.log(error)
            alert('error while making API call')
        } 
    },[loading, error, response])

    return (
        <div>
            <div className="container">
                <div className="col-md-6 offset-md-3">
                    <div className="card">
                        <div className="card-header text-center primary-background text-white">
                            <br />
                                   Add a News Here
                                </div>
                        <div className="card-body">

                                <div className="form-group">
                                    <label for="news-title">Title</label>
                                    <input onChange={(e) => {
                                    setTitle(e.target.value)
                                }} type="text" className="form-control" placeholder="Enter Title" />
                                </div>
                                <div className="form-group">
                                    <label for="news-description">Description</label>
                                    <textarea onChange={(e) => {
                                    setDescription(e.target.value)
                                }} className="form-control" rows="5" placeholder="Enter Description"></textarea>
                                </div>
                                <div className="form-group">
                                    <label for="news-date">Date</label>
                                    <input onChange={(e) => {
                                    setDate(e.target.value)
                                }} type="date" className="form-control" placeholder="Enter Date" />
                                </div>  
                                <button onClick={onAdd} type="submit" className="btn btn-primary" style={{ 'margin-top': '20px' }}>Add News</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const style = {
    margin: '8px'
}

export default AddNewsScreen