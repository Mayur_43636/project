package com.app.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.CategoryRepository;
import com.app.dao.NewsRepository;
import com.app.dto.NewsDTO;
import com.app.pojos.Category;
import com.app.pojos.News;


@Service
@Transactional
public class NewsServiceImpl implements INewsService {
	
	//dependency
	@Autowired
	private NewsRepository newsRepo;
			
	
	@Override
	public News addNewsDetails(NewsDTO newsDTO) {
		News news = new News();
		BeanUtils.copyProperties(newsDTO, news);
		System.out.println("add news src "+newsDTO);
		return newsRepo.save(news);
	}


	@Override
	public List<News> getNewsdetails() {
		System.out.println("get news details ");
		return newsRepo.findAll();
	}
	
	@Override
	public void deleteNews(int id) {
		newsRepo.deleteById(id);
	}


}
