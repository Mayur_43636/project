import { createStore, combineReducers, applyMiddleware } from 'redux'
import { userDetailsReducer, userSigninReducer, userSignupReducer, userUpdateProfileReducer } from './reducers/userReducers'
import { composeWithDevTools } from 'redux-devtools-extension'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import { addblogReducer, fetchblogReducer, statusblogReducer,deleteblogReducer } from './reducers/blogReducer'
import { addNewsReducer, deleteNewsReducer, fetchNewsReducer } from './reducers/newsReducer'
import { addCategoryReducer, deleteCategoryReducer, fetchCategoryReducer } from './reducers/categoryReducer'
import { addWebinarReducer, deleteWebinarReducer, fetchWebinarReducer } from './reducers/webinarReducers'
import { addCommentReducer, fetchCommentReducer } from './reducers/commentReducers'

//combine reducer 
const reducers = combineReducers({
    userSignup : userSignupReducer,
    userSignin : userSigninReducer,
    addblog : addblogReducer,
    getblog : fetchblogReducer,
    deleteBlog:deleteblogReducer,
    addNews : addNewsReducer,
    getNews : fetchNewsReducer,
    deleteNews : deleteNewsReducer,
    addCategory : addCategoryReducer,
    getCategory : fetchCategoryReducer,
    deleteCategory : deleteCategoryReducer,
    getWebinars : fetchWebinarReducer,
    addWebinar : addWebinarReducer,
    deleteWebinar : deleteWebinarReducer,
    getUser : userDetailsReducer,
    statusChange : statusblogReducer,
    getComments : fetchCommentReducer,
    addComment : addCommentReducer,
    updateProfile :  userUpdateProfileReducer

})

const store = createStore(
    reducers, 
    composeWithDevTools(applyMiddleware(logger, thunk))
)

export default store