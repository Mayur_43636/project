package com.app.service;

import java.util.List;

import com.app.dto.CategoryDTO;
import com.app.pojos.Category;

public interface ICategoryService {
	Category addCategoryDetails(CategoryDTO categoryDTO);
	List<Category> getCategorydetails();
	void deleteCategory(int id);
}
