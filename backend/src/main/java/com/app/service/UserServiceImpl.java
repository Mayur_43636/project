package com.app.service;



import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.UserRepository;
import com.app.dto.UserDTO;
import com.app.pojos.Role;
import com.app.pojos.User;

@Service
@Transactional
public class UserServiceImpl implements IUserService{

	//dependency
	@Autowired
	private UserRepository userRepo;
	
	@Override
	public User addUserDetails(UserDTO userDTO) {
		userDTO.setRole(Role.BLOGGER);
		User user = new User();
		BeanUtils.copyProperties(userDTO, user);
		System.out.println("add user src "+userDTO);
		return userRepo.save(user);
	}

	@Override
	public User getUserDetails(String email, String password) {
		System.out.println("in userService "+email+password);
		return userRepo.findByEmailAndPassword(email, password).orElseThrow(() -> new RuntimeException("User not found"));
	}

	@Override
	public User getUserDetails(int token) {
		Optional<User> optionalUser=userRepo.findById(token);
		return optionalUser.isPresent() ? optionalUser.get() : null;
	}

	@Override
	public String updateUserDetails(User user) {
		System.out.println("in update" +user);
		userRepo.save(user);
		return "User details updated";
	}
	
}
