import { Link } from "react-router-dom"
import Header from "../components/Header"
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from "react"
import { getWebinars } from "../actions/webinarActions"

const WebinarScreen = () => {

    const dispatch = useDispatch()
    const webinars = useSelector((store) => store.getWebinars)
    const { loading, response, error } = webinars

    //call once when page loaded successfully
    useEffect(() => {
        dispatch(getWebinars())
    }, [])

    useEffect(() => { }, [loading, response, error])
    //console.log(window.location.pathname)

    return (
        <div className="container">
            <div className="row">
                <Header title="Webinar" />
                {response && response.result && response.result.length > 0 &&
                    response.result.map((webinar) => {
                        return (
                            <div className="webinar col-md-3">
                                <div className="title">{webinar.title}</div>
                                <div className="description">{webinar.description}</div>
                                <div className="date">date: {webinar.date}</div>
                                { (sessionStorage['user_role'] == 'BLOGGER') &&
                                    <Link to="/register_for_webinar">
                                        <button
                                            className="btn btn-sm btn-success btn-add-to-cart" style={buttonStyle}>
                                            Register
                                 </button>
                                    </Link>
                                }
                            </div>
                        )
                    })}
            </div>
        </div>
    )
}

const buttonStyle = {
    margin: '10px'
}

export default WebinarScreen