import {
    NEWS_ADD_FAIL,
    NEWS_ADD_REQUEST,
    NEWS_ADD_SUCCESS,
    NEWS_FETCH_FAIL,
    NEWS_FETCH_REQUEST,
    NEWS_FETCH_SUCCESS,
    NEWS_DELETE_FAIL, 
    NEWS_DELETE_REQUEST,  
    NEWS_DELETE_SUCCESS,
  } from '../constants/newsConstants'
  import axios from 'axios'
  
  export const addNews = (id,title,description,date) => {
    return (dispatch) => {
      dispatch({
        type: NEWS_ADD_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const body = {
        id,
        title,
        description,
        date,
      }
      const url = 'http://localhost:8080/news/add'
      axios
        .post(url, body, header)
        .then((response) => {
          dispatch({
            type: NEWS_ADD_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: NEWS_ADD_FAIL,
            payload: error,
          })
        })
    }
  }
  
  export const getNews = () => {
    return (dispatch) => {
      dispatch({
        type: NEWS_FETCH_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const url = 'http://localhost:8080/news/'
      axios
        .get(url, header)
        .then((response) => {
          dispatch({
            type: NEWS_FETCH_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: NEWS_FETCH_FAIL,
            payload: error,
          })
        })
    }
  }
  

  //delete news action
  export const deleteNews = (id) => {
    return (dispatch) => {
      dispatch({
        type: NEWS_DELETE_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }

      const url = 'http://localhost:8080/news/delete/'+id
      axios
        .delete(url, header)
        .then((response) => {
          dispatch({
            type: NEWS_DELETE_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: NEWS_DELETE_FAIL,
            payload: error,
          })
        })
    }
  }