package com.app.service;

import java.util.List;

import com.app.dto.WebinarDTO;
import com.app.pojos.Webinar;

public interface IWebinarService {
	Webinar addWebinarDetails(WebinarDTO webDTO);
	List<Webinar> getWebinardetails();
	void deleteWebinar(int id);
}
