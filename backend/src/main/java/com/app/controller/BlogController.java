package com.app.controller;

import java.io.File;

import java.io.IOException;

import java.io.FileInputStream;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.dto.ResponseDTO;
import com.app.pojos.Blog;
import com.app.pojos.Status;
import com.app.service.IBlogService;
import com.app.service.IUserService;

@RestController
@RequestMapping("/blog")
@CrossOrigin
public class BlogController {
	// file path
	@Value("${file.upload.location}")
	private String location;

	@Autowired
	private IBlogService blogService;

	@Autowired
	private IUserService userService;

	@GetMapping()
	private ResponseDTO<?> getBlogContent() {
		System.out.println("in get blog details ");
		return new ResponseDTO<>(HttpStatus.OK, "Blog Contents added", blogService.getBlogContent());
	}

	@DeleteMapping("/delete/{id}")
	private ResponseDTO<?> DeleteBlogContent(@PathVariable int id) {
		blogService.deleteBlog(id);
		return new ResponseDTO<>(HttpStatus.OK, "Blog Deleted", "");
	}

	@GetMapping("/submitted")
	private ResponseDTO<?> getSubmittedBlogs() {
		System.out.println("in blog controller");
		return new ResponseDTO<>(HttpStatus.OK, "Submitted blogs", blogService.getSubmittedBlogs());
	}

	@PutMapping("/status/{id}/{status}")
	private ResponseDTO<?> changeStatus(@PathVariable int id, @PathVariable String status) {
		System.out.println("in change status ");
		return new ResponseDTO<>(HttpStatus.OK, "status changed", blogService.changeStatus(id, status));
	}

	// for posting the blog
	@PostMapping("/post")
	public ResponseDTO<?> blogAdd(@RequestParam MultipartFile imageFile, @RequestParam String title,
			@RequestParam String description, @RequestParam String content, @RequestParam String category,
			@RequestHeader int token) {
		Blog blog = new Blog();
		try {
			String newFilename = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg";
			imageFile.transferTo(new File(location, newFilename));
			System.out.println("new file name : " + newFilename);
			blog.setTitle(title);
			blog.setCategory(category);
			blog.setDescription(description);
			blog.setContent(content);
			blog.setPhoto(newFilename);
			blog.setDate(LocalDate.now());
			blog.setStatus(Status.VERIFYING);
			System.out.println("blog : " + blog);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseDTO<>(HttpStatus.OK, "success", blogService.blogAdd(token, blog));
	}

	@GetMapping("/published")
	private ResponseDTO<?> getApprovedBlogs() {
		return new ResponseDTO<>(HttpStatus.OK, "Approved blogs", blogService.getApprovedBlogs());
	}

	@GetMapping("/image/{imgName}")
	// Sample URL : http://localhost:8080/image/imageName.extension
	public ResponseEntity<InputStreamResource> getImage(@PathVariable String imgName) throws IOException {

		System.out.println("in img download 2 " + (location + imgName));
		Path path = Paths.get(location, imgName);
		InputStreamResource inputStreamResource = new InputStreamResource(new FileInputStream(path.toFile()));

		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type", Files.probeContentType(path));
		return ResponseEntity.ok().headers(headers).body(inputStreamResource);
	}

	@GetMapping("/{category}")
	public ResponseDTO<?> getBlogsByCategory(@PathVariable String category) {
		return new ResponseDTO<>(HttpStatus.OK, category, blogService.getBlogsByCategory(category));
	}
	
	@GetMapping("/by/{id}")
	public ResponseDTO<?> getBlogsById(@PathVariable int id) {
		return new ResponseDTO<>(HttpStatus.OK, "success", blogService.getBlogById(id));
	}
	
	
	@GetMapping("/published/by/{id}")
	private ResponseDTO<?> getPublishedBlogByUser(@PathVariable int id) {
		return new ResponseDTO<>(HttpStatus.OK, "Approved blogs", blogService.getPublishedBlogByUser(id));
	}
}