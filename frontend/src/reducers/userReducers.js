import { USER_SIGNIN_FAIL, 
    USER_SIGNIN_REQUEST, 
    USER_SIGNIN_SUCCESS, 
    USER_SIGNUP_FAIL, 
    USER_SIGNUP_REQUEST, 
    USER_SIGNUP_SUCCESS,
    USER_SIGNOUT,
    
  USER_DETAILS_FAIL,
  USER_DETAILS_REQUEST,
  USER_DETAILS_RESET,
  USER_DETAILS_SUCCESS,  
  
  USER_UPDATE_PROFILE_FAIL,
  USER_UPDATE_PROFILE_REQUEST,
  USER_UPDATE_PROFILE_SUCCESS,
  USER_UPDATE_PROFILE_RESET,
} from '../constants/userConstants'

export const userSignupReducer = (state = {},action) => {
    switch(action.type){
        case USER_SIGNUP_REQUEST : 
            return { loading : true}
        case USER_SIGNUP_SUCCESS :
            return { loading : false, response : action.payload }
        case USER_SIGNUP_FAIL : 
            return { loading : false, error : action.payload }
        default :
            return state
    }
}

export const userSigninReducer = (state = {},action) => {
    switch(action.type){
        case USER_SIGNIN_REQUEST : 
            return { loading : true}
        case USER_SIGNIN_SUCCESS :
            return { loading : false, response : action.payload }
        case USER_SIGNIN_FAIL : 
            return { loading : false, error : action.payload }
        case USER_SIGNOUT:
            return {}
        default :
            return state
    }
}



export const userDetailsReducer = (state = {}, action) => {
  switch (action.type) {
    case USER_DETAILS_REQUEST:
      return {  loading: true };
    case USER_DETAILS_SUCCESS:
      return { loading: false, response: action.payload };
    case USER_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case USER_DETAILS_RESET:
      return { };
    default:
      return state;
  }
};

export const userUpdateProfileReducer = (state = {}, action) => {
  switch (action.type) {
    case USER_UPDATE_PROFILE_REQUEST:
      return { loading: true };
    case USER_UPDATE_PROFILE_SUCCESS:
      return { loading: false, response : action.payload };
    case USER_UPDATE_PROFILE_FAIL:
      return { loading: false, error: action.payload };
    case USER_UPDATE_PROFILE_RESET:
      return {};
    default:
      return state;
  }
};