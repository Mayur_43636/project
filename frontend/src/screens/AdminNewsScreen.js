import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { getNews,deleteNews } from '../actions/newsActions'

const AdminNewsScreen = (props) => {

    const dispatch = useDispatch()
    const news = useSelector((store) => store.getNews)
    const { loading, response, error } = news

    const deletenews = useSelector((store) => store.deleteNews)

    //call once when page loaded successfully
    useEffect(() => {
        dispatch(getNews())
    }, [])

    useEffect(() => { }, [loading, response, error])

    useEffect(() => {
        console.log('use effect called for delete news')
        console.log(deletenews.response)
         //if delete news is success rerender news list
        if(deletenews.response && (deletenews.response.status == 'OK')){
         console.log(deletenews.response.result)
         dispatch(getNews())
     }
     }, [deletenews.loading,deletenews.response,])
 
     const onDelete = (id) => {
         dispatch(deleteNews(id))
         console.log('ondelete')
     } 

    return (
        <div>
            <div className="container text-center" style={{ 'margin-top': '30px' }}>
                <Link to="/add_news">
                    <button className="btn btn-primary">Add news</button>
                </Link>
            </div>
            <div className="container">
                <div className="row">
                    {response && response.result && response.result.length > 0 &&
                        response.result.map((news) => {
                        return (
                            <div className="news col-md-3">
                                <div className="title">{news.title}</div>
                                <div className="description">{news.description}</div>
                                <div className="date">date: {news.date}</div>
                                <Link to={{
                                        pathname: "/update_news",
                                        state: news
                                }}>
                                <button
                                    className="btn btn-sm btn-success btn-update" style={buttonStyle}>
                                    Update
                                </button>
                                </Link>
                                <button onClick={() => {
                                        onDelete(news.id)
                                    }}
                                    className="btn btn-sm btn-success btn-delete" style={buttonStyle}>
                                    Delete
                                </button>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}

const buttonStyle = {
    margin: '10px'
}
export default AdminNewsScreen
