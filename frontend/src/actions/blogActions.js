import {
    BLOG_ADD_FAIL,
    BLOG_ADD_REQUEST,
    BLOG_ADD_SUCCESS,
    BLOG_FETCH_FAIL,
    BLOG_FETCH_REQUEST,
    BLOG_FETCH_RESET,
    BLOG_FETCH_SUCCESS,
    BLOG_STATUS_FAIL,
    BLOG_STATUS_REQUEST,
    BLOG_STATUS_RESET,
    BLOG_STATUS_SUCCESS,
    DELETE_BLOG_REQUEST,
    DELETE_BLOG_SUCCESS,
    DELETE_BLOG_FAILURE
  } from '../constants/blogConstants'
  import axios from 'axios'

  //delete Blog
  export const deleteBlog= (id) => {
    return (dispatch) => {
      dispatch({ type: DELETE_BLOG_REQUEST
      })

      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
    
      const url = 'http://localhost:8080/blog/delete/'+id
      axios
      .delete(url, header)
      .then((response) => {
        dispatch({
          type:DELETE_BLOG_SUCCESS,
          payload: response.data,
        })
      })
      .catch((error) => {
        dispatch({
          type:DELETE_BLOG_FAILURE,
          payload: error,
        })
      })
    }
  }
  
  export const addblog = (formdata) => {
    return (dispatch) => {
      dispatch({
        type: BLOG_ADD_REQUEST,
      })
  
      const header = {
        headers: {
          token: sessionStorage['token'],
        },
      }
      
      const url = 'http://localhost:8080/blog/post'
      axios
        .post(url, formdata, header)
        .then((response) => {
          dispatch({
            type: BLOG_ADD_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: BLOG_ADD_FAIL,
            payload: error,
          })
        })
    }
  }
  
  export const getblog = () => {
    return (dispatch) => {
      dispatch({
        type: BLOG_FETCH_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const url = 'http://localhost:8080/blog/'
      axios
        .get(url, header)
        .then((response) => {
          dispatch({
            type: BLOG_FETCH_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: BLOG_FETCH_FAIL,
            payload: error,
          })
        })
    }
  }
  
  export const changeStatus = (id,status) => {
    return (dispatch) => {
      dispatch({
        type: BLOG_STATUS_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const url = 'http://localhost:8080/blog/status/'+id+'/'+status
      axios
        .put(url, header)
        .then((response) => {
          dispatch({
            type: BLOG_STATUS_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: BLOG_STATUS_FAIL,
            payload: error,
          })
        })
    }
  }

  export const resetStatus = () => {
    return (dispatch) => {
      dispatch({ type: BLOG_STATUS_RESET })
      //document.location.href = '/manage_blog'
    }
  }

  export const getSubmittedBlog = () => {
    return (dispatch) => {
      dispatch({
        type: BLOG_FETCH_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const url = 'http://localhost:8080/blog/submitted'
      axios
        .get(url, header)
        .then((response) => {
          dispatch({
            type: BLOG_FETCH_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: BLOG_FETCH_FAIL,
            payload: error,
          })
        })
    }
  }

  export const getPublishedBlog = () => {
    return (dispatch) => {
      dispatch({
        type: BLOG_FETCH_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const url = 'http://localhost:8080/blog/published'
      axios
        .get(url, header)
        .then((response) => {
          dispatch({
            type: BLOG_FETCH_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: BLOG_FETCH_FAIL,
            payload: error,
          })
        })
    }
  }

  export const getBlogByCategory = (category) => {
    return (dispatch) => {
      dispatch({
        type: BLOG_FETCH_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const url = 'http://localhost:8080/blog/'+category
      axios
        .get(url, header)
        .then((response) => {
          dispatch({
            type: BLOG_FETCH_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: BLOG_FETCH_FAIL,
            payload: error,
          })
        })
    }
  }

  export const resetBlogByCategory = () => {
    return (dispatch) => {
      dispatch({ type: BLOG_FETCH_RESET })
      //document.location.href = '/blog_category'
    }
  }

  export const getBlogById = (id) => {
    return (dispatch) => {
      dispatch({
        type: BLOG_FETCH_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const url = 'http://localhost:8080/blog/by/'+id
      axios
        .get(url, header)
        .then((response) => {
          dispatch({
            type: BLOG_FETCH_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: BLOG_FETCH_FAIL,
            payload: error,
          })
        })
    }
  }


  export const getPublishedBlogByUser = (id) => {
    return (dispatch) => {
      dispatch({
        type: BLOG_FETCH_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
        },
      }
  
      const url = 'http://localhost:8080/blog/published/by/'+id
      axios
        .get(url, header)
        .then((response) => {
          dispatch({
            type: BLOG_FETCH_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: BLOG_FETCH_FAIL,
            payload: error,
          })
        })
    }
  }