package com.app.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.WebinarRepository;
import com.app.dto.ResponseDTO;
import com.app.dto.WebinarDTO;
import com.app.pojos.Webinar;

@Service
@Transactional
public class WebinarServiceImpl implements IWebinarService {

	@Autowired
	private WebinarRepository webRepo;
	
	@Override
	public Webinar addWebinarDetails(WebinarDTO webDTO) {
		Webinar webinar = new Webinar();
		BeanUtils.copyProperties(webDTO, webinar);
		System.out.println("add webinar src "+webDTO);
		return webRepo.save(webinar);
	}

	@Override
	public List<Webinar> getWebinardetails() {
		System.out.println("get webinar details ");
		return webRepo.findAll();
	}

	@Override
	public void deleteWebinar(int id) {
		webRepo.deleteById(id);
	}

}
