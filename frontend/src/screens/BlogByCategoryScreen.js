import { useEffect, useLayoutEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getBlogByCategory, resetBlogByCategory } from '../actions/blogActions'
import Header from '../components/Header'
import { Link } from 'react-router-dom'

const BlogsByCategory = (props) => {
    
    const category = props.location.state
    const dispatch = useDispatch()
    const blogs = useSelector((store) => store.getblog)
    const { loading, response, error } = blogs

    
    //call once when page is loaded
    useEffect(() => {
        dispatch(getBlogByCategory(category))
    }, [category])

    
    useEffect(() => {
        //window.location.reload(false);
    }, [loading, response, error])

    const imageUrl = "http://localhost:8080/blog/image/"

    return (
        <div class="main">
            <Header title={category + ' Blogs'} />
            <ul class="cards">
                {response && response.result && response.result.length > 0 &&
                    response.result.map((blog) => {
                        return (
                            <li class="cards_item">
                                <div class="card">
                                    <div class="card_image"><img src={imageUrl + blog.photo} /></div>
                                    <div class="card_content">
                                        <h2 class="card_title">{blog.title}</h2>
                                        <p class="card_text">{blog.description}</p>
                                        <Link to="/blog_display">
                                        <button onClick={() => sessionStorage.setItem('blog_id',blog.id)} class="neha card_btn">Read More</button>
                                        </Link>
                                    </div>
                                </div>
                            </li>
                        )
                    })}
            </ul>
        </div>
    )
}

export default BlogsByCategory