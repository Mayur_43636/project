package com.app.service;

import java.util.List;
import java.util.Optional;

import com.app.dto.BlogDTO;
import com.app.pojos.Blog;
import com.app.pojos.Comment;

public interface IBlogService {

	List<BlogDTO> getBlogContent();

	void deleteBlog(int id);

	List<BlogDTO> getSubmittedBlogs();

	String changeStatus(int id, String status);

	String blogAdd(int id, Blog blog);

	List<BlogDTO> getApprovedBlogs();

	List<BlogDTO> getBlogsByCategory(String category);
	
	BlogDTO getBlogById(int id);
	
	List<Comment> getCommentList(int id);
	
	List<BlogDTO> getPublishedBlogByUser(int id);
}
