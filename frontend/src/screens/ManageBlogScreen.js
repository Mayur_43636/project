import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { changeStatus, getSubmittedBlog } from "../actions/blogActions"

const ManageBlogScreen = () => {


    const dispatch = useDispatch()
    const blogs = useSelector((store) => store.getblog)
    const status = useSelector((store) => store.statusChange)
    const { loading, response, error } = blogs

    //call once when page is loaded
    useEffect(() => {
        dispatch(getSubmittedBlog())
    }, [])

    useEffect(() => {
       
    }, [loading, response, error])

    //console.log('response',response)


    const onApprove = (id) => {
        dispatch(changeStatus(id,'APPROVED'))
    }

    const onReject = (id) => {
        dispatch(changeStatus(id,'REJECTED'))
    }

    useEffect(() => {
        dispatch(getSubmittedBlog())
    },[status.loading, status.response, status.error])

    return (
        <div className="container" style={style}>
            <h1>Manage Blogs</h1>
            <table className="table table-hover table-bordered shadow p-3 mb-5 bg-body rounded" >
                <thead className="primary-background">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Title</th>
                        <th scope="col">Category</th>
                        <th scope="col">Date Created</th>
                        <th scope="col">Read details</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {response && response.result && response.result.length > 0 &&
                        response.result.map((blog) => {
                            return (
                                <>
                                    <tr>
                                        <th scope="row">{blog.id}</th>
                                        <td>
                                            <Link to="/blog">
                                                {blog.title}
                                            </Link>
                                        </td>
                                        <td>{blog.category}</td>
                                        <td>{blog.date}</td>
                                        <td><Link to="/blog_display">
                                        <button onClick={() => sessionStorage.setItem('blog_id',blog.id)} className="btn btn-outline-success" data-mdb-ripple-color="dark">Read More</button>
                                        </Link></td>
                                        <td><button onClick={() => {
                                            onApprove(blog.id)
                                        }} type="button" className="btn btn-outline-success" data-mdb-ripple-color="dark" style={buttonStyle}>
                                            Approve
                                    </button>
                                            <button onClick={() => {
                                                onReject(blog.id)
                                            }} type="button" className="btn btn-outline-danger" data-mdb-ripple-color="dark" style={buttonStyle}>
                                                Reject
                                    </button>
                                        </td>
                                    </tr>
                                </>
                            )
                        })}
                </tbody>
            </table>
        </div>
    )
}

const style = { 'margin-top': '20px' }
const buttonStyle = {
    margin: '10px'
}

export default ManageBlogScreen