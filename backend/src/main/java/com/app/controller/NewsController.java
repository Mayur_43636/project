package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.NewsDTO;
import com.app.dto.ResponseDTO;
import com.app.service.INewsService;

@RestController
@RequestMapping("/news")
@CrossOrigin
public class NewsController {
	//dependency
		@Autowired
		private INewsService newsService;
		
		@PostMapping("/add")
		private ResponseDTO<?> addNewsDetails(@RequestBody NewsDTO newsDTO){
			System.out.println("in add news details ");
			return new ResponseDTO<>(HttpStatus.OK,"News details added",newsService.addNewsDetails(newsDTO));
		}
		
		@GetMapping()
		private ResponseDTO<?> getNewsDetails(){
			System.out.println("in get news details ");
			return new ResponseDTO<>(HttpStatus.OK,"news details",newsService.getNewsdetails());
		}
		
		@DeleteMapping("/delete/{id}")
		private ResponseDTO<?> deleteNewsDetails(@PathVariable int id){
			System.out.println("in delete news details");
			newsService.deleteNews(id);
			return new ResponseDTO<>(HttpStatus.OK, "news deleted", "");
		}
}
