const Header = (props) => {
    return (
        <h1 className="title">{props.title}</h1>
    )
}

Header.defaultProps = {
    title : ''
}

export default Header