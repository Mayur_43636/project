package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.pojos.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer>{
	@Query("select c from Comment c left outer join fetch c.blogs where c.blogs.id=:id")
	List<Comment> getListComment(int id);
}
