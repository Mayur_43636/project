import { Link } from 'react-router-dom'
import { useEffect, useState } from 'react'
import * as FaIcons from 'react-icons/fa'
import * as AiIcons from 'react-icons/ai'
import { AdminSidebarData } from './AdminSidebar'
import { BloggerSidebarData } from './BloggerSidebar'

import './Sidebar.css'
import { useDispatch, useSelector } from 'react-redux'
import { logout } from '../actions/userActions'
import { getCategory } from '../actions/categoryActions'

const Navigation = () => {

    const [sidebar, setSidebar] = useState(false)

    const showSidebar = () => setSidebar(!sidebar)

    const role = sessionStorage['user_role']

    const userSignin = useSelector((store) => store.userSignin)
    const { loading, response, error } = userSignin

    useEffect(() => { }, [loading, response, role])
    const dispatch = useDispatch()

    const onLogout = () => {
        dispatch(logout())
    }

    //for category
    const categories = useSelector((store) => store.getCategory)
    useEffect(() => {
        dispatch(getCategory())
    }, [])

    useEffect(() => {
    }, [categories.loading, categories.response, categories.error])
    console.log('here', categories.response)

    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark primary-background" >
                <div className="navbar">
                    {(response || role) &&
                        <div className="sidebar">
                            <Link to="#" className='menu-bars'>
                                <FaIcons.FaBars onClick={showSidebar} />
                            </Link>
                        </div>}
                </div>
                <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
                    <ul className="sidebarMenuInner" onClick={showSidebar}>
                        <li className="navbar-toggle">
                            Dashboard
                            <Link to="#" className="menu-bars">
                                <AiIcons.AiOutlineClose />
                            </Link>
                        </li>

                        {(role == 'ADMIN') && AdminSidebarData.map((item, index) => {
                            return (
                                <li key={index} className={item.cName}>
                                    <Link to={item.path}>
                                        {item.icon}
                                        <span>{item.title}</span>
                                    </Link>
                                </li>
                            )
                        })
                        }
                        {(role == 'BLOGGER') && BloggerSidebarData.map((item, index) => {
                            return (
                                <li key={index} className={item.cName}>
                                    <Link to={item.path}>
                                        {item.icon}
                                        <span>{item.title}</span>
                                    </Link>
                                </li>
                            )
                        })}


                    </ul>

                </nav>
                <Link to="/">
                    <a className="navbar-brand"> <span></span> Home</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                </Link>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <Link to="/news">
                                <a className="nav-link"> News</a>
                            </Link>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <span className="fa fa-check-square-o"></span> Categories
                                </a>


                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                {categories.response && categories.response.result.length > 0 &&
                                    categories.response.result.map((category) => {
                                        return (
                                            <Link to={{
                                                pathname: "/blog_category",
                                                state: category.title
                                            }}>
                                                <li><a className="dropdown-item" >{category.title}</a></li>
                                            </Link>
                                        )
                                    })
                                }
                            </ul>
                        </li>
                        <li className="nav-item">
                            <Link to="/webinar">
                                <a className="nav-link">  Webinar</a>
                            </Link>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Skill test
                                    </a>
                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li>
                                    <Link to="skill_test_java">
                                        <a className="dropdown-item" >Java</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="skill_test_react">
                                        <a className="dropdown-item" >React</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="skill_test_data_structure">
                                        <a className="dropdown-item" >Data Structure</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="skill_test_os">
                                        <a className="dropdown-item" >Operating System</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="skill_test_spring">
                                        <a className="dropdown-item" >Spring</a>
                                    </Link>
                                </li>
                            </ul>
                        </li>




                        <li className="nav-item">
                            <Link to="/about_us">
                                <a className="nav-link">  About US</a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/contact_us">
                                <a className="nav-link">  Contact US</a>
                            </Link>
                        </li>
                        {(loading || !role) &&
                            <li className="nav-item">
                                <Link to="/login">
                                    <a className="nav-link">  Login</a>
                                </Link>
                            </li>
                        }
                        {(response || role) &&
                            <li className="nav-item">
                                <button onClick={onLogout} className="nav-link logout"> Logout</button>
                            </li>
                        }
                    </ul>
                    <form className="d-flex">
                        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                        <button className="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>
        </div>
    )
}

export default Navigation