import { Link } from "react-router-dom"
import Header from "../components/Header"
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from "react"
import { getCategory } from "../actions/categoryActions"

const CategoriesScreen = () => {

    const dispatch = useDispatch()
    const category = useSelector((store) => store.getCategory)
    const { loading, response, error } = category

    //call once when page loaded successfully
    useEffect(() => {
        dispatch(getCategory())
    }, [])

    useEffect(() => { }, [loading, response, error])
    //console.log(window.location.pathname)

    return (
        <div className="container">
        <div className="row">
            <Header title="Category" />
            {response && response.result && response.result.length > 0 &&
                response.result.map((category) => {
                    return (
                        <div className="category col-md-3">
                            <div className="title">{category.title}</div>
                            <div className="description">{category.description}</div>
                        </div>
                    )
                })}
        </div>
    </div>
)
}

const buttonStyle = {
margin: '10px'
}

export default CategoriesScreen