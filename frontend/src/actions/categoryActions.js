import {
  CATEGORY_ADD_FAIL,
  CATEGORY_ADD_REQUEST,
  CATEGORY_ADD_SUCCESS,
  CATEGORY_FETCH_FAIL,
  CATEGORY_FETCH_REQUEST,
  CATEGORY_FETCH_SUCCESS,
  CATEGORY_DELETE_FAIL, 
  CATEGORY_DELETE_REQUEST, 
  CATEGORY_DELETE_SUCCESS,
} from '../constants/categoryConstants'
import axios from 'axios'

export const addCategory = (id,title,description) => {
  return (dispatch) => {
    dispatch({
      type: CATEGORY_ADD_REQUEST,
    })

    const header = {
      headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
      },
  }

    const body = {
      id,
      title,
      description,
    }
    const url = 'http://localhost:8080/category/add'
    axios
      .post(url, body, header)
      .then((response) => {
        dispatch({
          type: CATEGORY_ADD_SUCCESS,
          payload: response.data,
        })
      })
      .catch((error) => {
        dispatch({
          type: CATEGORY_ADD_FAIL,
          payload: error,
        })
      })
  }
}

export const getCategory = () => {
  return (dispatch) => {
    dispatch({
      type: CATEGORY_FETCH_REQUEST,
    })

    const header = {
      headers: {
          'Content-Type': 'application/json',
          token: sessionStorage['token'],
      },
  }
  
    const url = 'http://localhost:8080/category'
    axios
      .get(url, header)
      .then((response) => {
        dispatch({
          type: CATEGORY_FETCH_SUCCESS,
          payload: response.data,
        })
      })
      .catch((error) => {
        dispatch({
          type: CATEGORY_FETCH_FAIL,
          payload: error,
        })
      })
  }
}

//delete category action
export const deleteCategory = (id) => {
  return (dispatch) => {
    dispatch({
      type: CATEGORY_DELETE_REQUEST,
    })

    const header = {
      headers: {
        'Content-Type': 'application/json',
        token: sessionStorage['token'],
      },
    }

    const url = 'http://localhost:8080/category/delete/'+id
    axios
      .delete(url, header)
      .then((response) => {
        dispatch({
          type: CATEGORY_DELETE_SUCCESS,
          payload: response.data,
        })
      })
      .catch((error) => {
        dispatch({
          type: CATEGORY_DELETE_FAIL,
          payload: error,
        })
      })
  }
}

