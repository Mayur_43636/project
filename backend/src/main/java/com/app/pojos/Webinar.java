package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "webinar")
public class Webinar extends BaseEntity {
	
	@Column(length = 50)
	private String title;
	@Column(length = 255)
	private String description;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate date;
	
	public Webinar() {
		System.out.println("in ctor of : "+getClass().getName());
	}

	public Webinar(String title, String description, LocalDate date) {
		super();
		this.title = title;
		this.description = description;
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Webinar [id=" + getId() + ", title=" + title + ", description=" + description + ", date=" + date + "]";
	}
}
