package com.app.dto;

import java.util.Arrays;

import com.app.pojos.Role;

public class UserDTO {
	private int id;
	private String name;
	private String email;
	private String password;
	private byte[] photo;
	private String gender;
	private String about;
	private Role role;
	
	public UserDTO() {
		System.out.println("in ctor of : "+getClass().getName());
	}

	public UserDTO(int id, String name, String email, String password, byte[] photo, String gender, String about,
			Role role) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.photo = photo;
		this.gender = gender;
		this.about = about;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", photo="
				+ Arrays.toString(photo) + ", gender=" + gender + ", about=" + about + ", role=" + role + "]";
	}

}
