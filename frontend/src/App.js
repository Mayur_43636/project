import './App.css'
import Navigation from './components/Navigation'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import EditProfileScreen from './screens/EditProfileScreen';
import ManageBlogScreen from './screens/ManageBlogScreen';
import CreateBlogScreen from './screens/CreateBlogScreen';
import NewsScreen from './screens/NewsScreen';
import AddNewsScreen from './screens/AddNewsScreen';
import AddCategoryScreen from './screens/AddCategoryScreen';
import CategoriesScreen from './screens/CategoriesScreen';
import WebinarScreen from './screens/WebinarScreen';
import AddWebinarScreen from './screens/AddWebinarScreen';
import ContactUsScreen from './screens/ContactUsScreen';
import RegisteredWebinarScreen from './screens/RegisteredWebinarScreen';
import AdminWebinarScreen from './screens/AdminWebinarScreen';
import RegisterForWebinarScreen from './screens/RegisterForWebinarScreen';
import HomePageScreen from './screens/HomePageScreen';
import AdminNewsScreen from './screens/AdminNewsScreen';
import AboutUsScreen from './screens/AboutUsScreen';
import JavaSkillTest from './screens/SkillTestScreens/JavaSkillTestScreen';
import AdminCategoriesScreen from './screens/AdminCategoriesScreen'
import ReactSkillTest from './screens/SkillTestScreens/ReactSkillTestScreen';
import DataStructureSkillTest from './screens/SkillTestScreens/DataStructureSkillTestScreen';
import OSSkillTest from './screens/SkillTestScreens/OSSkillTestScreen';
import SpringSkillTest from './screens/SkillTestScreens/SpringSkillTestScreen';
import UpdateWebinarScreen from './screens/UpdateWebinarScreen';
import UpdateNewsScreen from './screens/UpdateNewsScreen';
import UpdateCategoryScreen from './screens/UpdateCategoryScreen';
import ViewProfileScreen from './screens/ViewProfileScreen';
import TechScreen from './screens/Categories/TechScreen';
import TravelScreen from './screens/Categories/TravelScreen';
import LifeStyleScreen from './screens/Categories/LifeStyleScreen';
import PublishedBlogScreen from './screens/PublishedBlogScreen';
import BlogsByCategory from './screens/BlogByCategoryScreen';
import DisplayBlogScreen from './screens/DisplayBlogScreen';


function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <Navigation />
          <Switch>
            <Route path="/login" component={LoginScreen} />
            <Route path="/register" component={RegisterScreen} />
            <Route path="/edit_Profile" component={EditProfileScreen}></Route>
            <Route path="/manage_blog" component={ManageBlogScreen} />
            <Route path="/create_blog" component={CreateBlogScreen} />
            <Route path="/news" component={NewsScreen} />
            <Route path="/add_news" component={AddNewsScreen} />
            <Route path="/categories" component={CategoriesScreen} />
            <Route path="/add_category" component={AddCategoryScreen} />
            <Route path="/webinar" component={WebinarScreen} />
            <Route path="/add_webinar" component={AddWebinarScreen} />
            <Route path="/contact_us" component={ContactUsScreen}></Route> 
            <Route path="/registered_webinar" component={RegisteredWebinarScreen}></Route>
            <Route path="/admin_webinar" component={AdminWebinarScreen}></Route>
            <Route path="/register_for_webinar" component={RegisterForWebinarScreen}></Route>
            <Route path="/" exact component={HomePageScreen}/>
            <Route path="/admin_news" component={AdminNewsScreen}></Route>
            <Route path="/about_us" component={AboutUsScreen}></Route>
            <Route path="/admin_category" component={AdminCategoriesScreen}></Route>
            <Route path="/skill_test_java" component={JavaSkillTest}></Route>
            <Route path="/skill_test_react" component={ReactSkillTest}></Route>
            <Route path="/skill_test_data_structure" component={DataStructureSkillTest}></Route>
            <Route path="/skill_test_os" component={OSSkillTest}></Route>
            <Route path="/skill_test_spring" component={SpringSkillTest}></Route>
            <Route path="/update_webinar" component={UpdateWebinarScreen}></Route>
            <Route path="/update_news" component={UpdateNewsScreen}></Route>
            <Route path="/update_category" component={UpdateCategoryScreen}></Route>
            {/*<Route path="/view_profile" component={ViewProfileScreen}></Route>*/}
            <Route path="/tech_blogs" component={TechScreen}></Route>
            <Route path="/travel_blogs" component={TravelScreen}></Route>
            <Route path="/lifestyle_blogs" component={LifeStyleScreen}></Route>
            <Route path="/published_blogs" component={PublishedBlogScreen}/>
            <Route path="/blog_category" component={BlogsByCategory}/>
            <Route path="/blog_display" component={DisplayBlogScreen}/>
          </Switch>
        </div>
      </Router>
    </div>
  ); 
}

export default App;
